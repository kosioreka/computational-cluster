﻿using System;

namespace Algorithms
{
    public abstract class TaskSolver
    {
        protected byte[] _problemData;

        public TaskSolver(byte[] problemData)
        {
            _problemData = problemData;
        }

        public abstract string Name { get; }

        public abstract byte[][] DivideProblem(int nodeCount);
        public abstract byte[] MergeSolution(byte[][] solutions);
        public abstract byte[] Solve(byte[] partialData, TimeSpan timeout);
    }
}