﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Algorithms
{
    public class Solver : TaskSolver
    {
        struct DepotVehicle
        {
            public int x, y;
            public int open_time;
            public int close_time;
            public int vehicles_number;
            public int vehicle_capacity;
        }

        struct Depot
        {
            public int x, y;
            public int time_available;
            public int duration_time;
            public int demand;
        }

        struct SubProblem
        {
            public ulong perms_start;
            public ulong perms_count;
            public int non_unique;
        }

        struct ProblemTextHelper
        {
            public int DepotsCount;
            public int VisitsCount;
            public int LocationsCount;
        }

        struct PartialSolution
        {
            public double Minimum;
            public ulong PermutationNumber;
            public int VehiclePermutationNumber;
        }

        struct FinalSolution
        {
            public int[] Clients;
            public int[] Vehicles;
            public double Cost;
        }

        private readonly string[] _text;
        private Depot[] _clients;
        private DepotVehicle _depotVehicle;

        public Solver(byte[] problemData)
            : base(problemData)
        {
            _text = Encoding.UTF8.GetString(problemData).Split(new char[] { '\n', '\r' }, StringSplitOptions.RemoveEmptyEntries);
            LoadData();
        }

        public override string Name
        {
            get
            {
                return "DVRP";
            }
        }

        public override byte[][] DivideProblem(int nodeCount)
        {
            int subProblemsCount;
            var subProblemsData = Divide(_depotVehicle, _clients, _clients.Length, nodeCount, out subProblemsCount);
            var currentData = subProblemsData;

            SubProblem[] subProblems = new SubProblem[subProblemsCount];
            for (int i = 0; i < subProblemsCount; i++)
            {
                subProblems[i] = (SubProblem)Marshal.PtrToStructure(subProblemsData, typeof(SubProblem));
                subProblemsData += Marshal.SizeOf(typeof(SubProblem));
            }

            var subProblemSize = Marshal.SizeOf(typeof(SubProblem));

            byte[][] data = new byte[subProblemsCount][];

            for (int i = 0; i < subProblemsCount; i++)
            {
                byte[] subProblemData = new byte[subProblemSize];
                Marshal.Copy(currentData, subProblemData, 0, subProblemSize);
                data[i] = subProblemData;
                currentData += subProblemSize;
            }

            FreeMemory(subProblemsData);

            return data;
        }

        public override byte[] MergeSolution(byte[][] solutions)
        {
            var bestSolution = new PartialSolution();
            bestSolution.Minimum = double.MaxValue;

            foreach (var currentData in solutions)
            {
                var currentSolution = new PartialSolution();
                ByteArrayToSolution(currentData, ref currentSolution);
                if (currentSolution.Minimum < bestSolution.Minimum)
                {
                    bestSolution = currentSolution;
                }
            }

            var vehiclePermutation = new int[_depotVehicle.vehicles_number];
            var permutation = new int[_clients.Length];

            GetPermutations(_depotVehicle, _clients.Length, bestSolution.VehiclePermutationNumber, bestSolution.PermutationNumber, vehiclePermutation, permutation);

            var finalSolution = new FinalSolution();
            finalSolution.Cost = bestSolution.Minimum;
            finalSolution.Vehicles = vehiclePermutation;
            finalSolution.Clients = permutation;

            var resultString = CreateResponseString(finalSolution);

            return Encoding.UTF8.GetBytes(resultString);
        }

        public override byte[] Solve(byte[] subProblemData, TimeSpan timeout)
        {
            ulong permNumberMin;
            int vehiclePermNumberMin;

            var minimum = Cuda_Solve(_depotVehicle, _clients, _clients.Length, subProblemData, out permNumberMin, out vehiclePermNumberMin);

            var solution = new PartialSolution();
            solution.Minimum = minimum;
            solution.PermutationNumber = permNumberMin;
            solution.VehiclePermutationNumber = vehiclePermNumberMin;

            byte[] data = SolutionToByteArray(solution);

            return data;
        }

        private string CreateResponseString(FinalSolution finalSolution)
        {
            var result = string.Empty;
            result += string.Format("Cost: {0}\n", finalSolution.Cost);
            int i = 0;
            foreach (var item in finalSolution.Vehicles)
            {
                if (item == 0)
                    continue;
                for (int j = 0; j < item; j++, i++)
                {
                    if (j != 0)
                        result += ", ";
                    result += string.Format("{0}", finalSolution.Clients[i]);
                }
                result += "\n";
            }
            return result;
        }

        private void LoadData()
        {
            var header = new ProblemTextHelper();
            _depotVehicle = new DepotVehicle();

            char[] separator = new char[] { ' ' };
            int lineNum = 0;

            for (; lineNum < _text.Length; ++lineNum)
            {
                string line = _text[lineNum];

                if (line == "DATA_SECTION")
                    break;
                string[] headerValuePair = line.Split(separator);

                switch (headerValuePair[0])
                {
                    case "NUM_DEPOTS:":
                        header.DepotsCount = int.Parse(headerValuePair[1]);
                        if (header.DepotsCount != 1)
                            throw new Exception("NUM_DEPOTS");
                        break;
                    case "NUM_CAPACITIES:":
                        if (int.Parse(headerValuePair[1]) != 1)
                            throw new Exception("NUM_CAPACITIES");
                        break;
                    case "NUM_VISITS:":
                        header.VisitsCount = int.Parse(headerValuePair[1]);
                        break;
                    case "NUM_LOCATIONS:":
                        header.LocationsCount = int.Parse(headerValuePair[1]);
                        break;
                    case "NUM_VEHICLES:":
                        _depotVehicle.vehicles_number = int.Parse(headerValuePair[1]);
                        break;
                    case "CAPACITIES:":
                        if (headerValuePair.Length > 2)
                            throw new Exception("CAPACITIES");
                        _depotVehicle.vehicle_capacity = int.Parse(headerValuePair[1]);
                        break;
                    default:
                        break;
                }
            }

            lineNum++;

            _clients = new Depot[header.VisitsCount];

            separator = new char[] { ' ' };

            bool parse = true;
            while (parse && lineNum < _text.Length)
            {
                switch (_text[lineNum])
                {
                    case "DEMAND_SECTION":
                        ForEachLine(_text, ref lineNum, separator, header.VisitsCount, (string[] values) =>
                        {
                            int i = int.Parse(values[0]) - 1;
                            int demand = int.Parse(values[1]);
                            _clients[i].demand = demand;
                        });
                        break;
                    case "LOCATION_COORD_SECTION":
                        ForEachLine(_text, ref lineNum, separator, header.LocationsCount, (string[] values) =>
                        {
                            int i = int.Parse(values[0]) - 1;
                            if (i == -1)
                            {
                                _depotVehicle.x = int.Parse(values[1]);
                                _depotVehicle.y = int.Parse(values[2]);
                                return;
                            }
                            _clients[i].x = int.Parse(values[1]);
                            _clients[i].y = int.Parse(values[2]);
                        });
                        break;
                    case "DURATION_SECTION":
                        ForEachLine(_text, ref lineNum, separator, header.VisitsCount, (string[] values) =>
                        {
                            int i = int.Parse(values[0]) - 1;
                            int duration = int.Parse(values[1]);
                            _clients[i].duration_time = duration;
                        });
                        break;
                    case "DEPOT_TIME_WINDOW_SECTION":
                        ForEachLine(_text, ref lineNum, separator, header.DepotsCount, (string[] values) =>
                        {
                            int i = int.Parse(values[0]);
                            int from = int.Parse(values[1]);
                            int to = int.Parse(values[2]);
                            _depotVehicle.open_time = from;
                            _depotVehicle.close_time = to;
                        });
                        break;
                    case "TIME_AVAIL_SECTION":
                        ForEachLine(_text, ref lineNum, separator, header.VisitsCount, (string[] values) =>
                        {
                            int i = int.Parse(values[0]) - 1;
                            int time = int.Parse(values[1]);
                            _clients[i].time_available = time;
                        });
                        break;
                    case "EOF":
                        parse = false;
                        break;
                    default:
                        lineNum += 1;
                        break;
                }
            }
        }

        private void ForEachLine(string[] lines, ref int lineNum, char[] separator, int count, Action<string[]> action)
        {
            lineNum += 1;
            for (int i = 0; i < count; ++i)
            {
                action(lines[lineNum].Split(separator, StringSplitOptions.RemoveEmptyEntries));
                lineNum += 1;
            }
        }

        byte[] SolutionToByteArray(PartialSolution obj)
        {
            int len = Marshal.SizeOf(obj);
            byte[] arr = new byte[len];
            IntPtr ptr = Marshal.AllocHGlobal(len);
            Marshal.StructureToPtr(obj, ptr, true);
            Marshal.Copy(ptr, arr, 0, len);
            Marshal.FreeHGlobal(ptr);
            return arr;
        }

        void ByteArrayToSolution(byte[] bytearray, ref PartialSolution obj)
        {
            int len = Marshal.SizeOf(obj);
            IntPtr i = Marshal.AllocHGlobal(len);
            Marshal.Copy(bytearray, 0, i, len);
            obj = (PartialSolution)Marshal.PtrToStructure(i, obj.GetType());
            Marshal.FreeHGlobal(i);
        }

        private const string DllPath = "HelloWorld.dll";

        [DllImport(DllPath, EntryPoint = "Solve", CallingConvention = CallingConvention.Cdecl)]
        private static extern double Cuda_Solve(DepotVehicle dv, Depot[] ds, int dsn, byte[] subProblem, [Out] out ulong permNumberMin, [Out] out int vehiclePermNumberMin);

        [DllImport(DllPath, EntryPoint = "Divide", CallingConvention = CallingConvention.Cdecl)]
        private static extern IntPtr Divide(DepotVehicle dv, Depot[] ds, int dsn, int numberOfNodes, [Out] out int subProblemsCount);

        [DllImport(DllPath, EntryPoint = "FreeMemory", CallingConvention = CallingConvention.Cdecl)]
        private static extern double FreeMemory(IntPtr subProblems);

        [DllImport(DllPath, EntryPoint = "GetPermutations", CallingConvention = CallingConvention.Cdecl)]
        private static extern void GetPermutations(DepotVehicle dv, int dsn, int vehicle_permutation_number, ulong permutation_number, int[] vehicle_permutation, int[] permutation);
    }
}