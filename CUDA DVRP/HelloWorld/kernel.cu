#include "cuda_runtime.h"
#include "device_launch_parameters.h"

//#include <time.h>
#include <algorithm>
#include <stdio.h>
#include <vector>
#include <math_constants.h>
#include <thrust/device_vector.h>
#include <thrust/extrema.h>

using namespace std;

#define ExportDLL extern "C" __declspec(dllexport)
#define SWAP(a,b) do{t=(a);(a)=(b);(b)=t;} while(0)
#define ullint unsigned long long

void unrank(ullint rank, int n, int *vec)
{
	int t;
	ullint r;
	if (n < 1)
		return;
	for (ullint i = n; i > 0; --i)
	{
		r = rank % i;
		rank = rank / i;
		SWAP(vec[r], vec[i - 1]);
	}
}

void get_permutation(ullint rank, int n, int *vec)
{
	int i;
	for (i = 0; i < n; ++i)
	{
		vec[i] = i;
	}
	unrank(rank, n, vec);
}

typedef struct Depot
{
public:
	Depot(int x, int y, int time_available, int duration_time, int demand) :
		x(x),
		y(y),
		time_available(time_available),
		duration_time(duration_time),
		demand(demand)
	{
	}

	Depot()
	{
	}

	int x, y;
	int time_available;
	int duration_time;
	int demand;

} Depot;

typedef struct DepotVehicle
{
public:
	DepotVehicle(int x, int y, int open_time, int close_time, int vehicles_number, int vehicle_capacity) :
		x(x),
		y(y),
		open_time(open_time),
		close_time(close_time),
		vehicles_number(vehicles_number),
		vehicle_capacity(vehicle_capacity)
	{
	}

	DepotVehicle()
	{
	}

	int x, y;
	int open_time, close_time;
	int vehicles_number;
	int vehicle_capacity;

} DepotVehicle;

vector<vector<int>> NumberIntoParts(int x, int n, int y)
{
	vector<vector<int> > v;
	if (x <= 1 || n == 1)
	{
		if (x <= y)
		{
			v.resize(1);
			v[0].push_back(x);
		}
		return v;
	}
	for (int i = min(x, y); i >= 1; i--)
	{
		vector<vector<int> > w = NumberIntoParts(x - i, n - 1, i);
		for (int j = 0; j < w.size(); j++)
		{
			w[j].push_back(i);
			v.push_back(w[j]);
		}
	}
	return v;
}

vector<vector<int>> NumberIntoParts(int number, int parts)
{
	auto perms = NumberIntoParts(number, parts, number);
	vector<vector<int>> result;
	for (int i = 0; i < perms.size(); ++i)
	{
		int length = parts - perms[i].size();
		for (int j = 0; j < length; j++)
		{
			perms[i].push_back(0);
		}
	}
	return perms;
}

ullint Factorial(ullint n)
{
	ullint ret = 1;
	for (ullint i = 1; i <= n; ++i)
	{
		ret *= i;
	}
	return ret;
}

int* ConvertToIntArray(vector<vector<int>> vv, const int k)
{
	int* result = (int*)malloc(k * vv.size() * sizeof(int));
	int g = 0;
	for (int i = 0; i < vv.size(); ++i)
	{
		for (int j = 0; j < k; ++j)
		{
			result[g] = vv[i][j];
			++g;
		}
	}
	return result;
}

void GetPermuts(int* permuts, ullint perm_start, ullint perm_end, int dsn)
{
	ullint i;
	int j;
	for (i = perm_start, j = 0; i < perm_end; ++i, j += dsn)
	{
		get_permutation(i, dsn, permuts + j);
	}
}

__global__ void dvrpKernel(const int* perms, const Depot* ds, const DepotVehicle* dv,
	const int* dsn, const int* vd, const int* vpc, const double* minimum, const double* costs, double* results)
{
	const int premutation_number = threadIdx.x;
	const int vehicle_permutation = blockIdx.x;
	const int* depot_permutation = perms + premutation_number * (unsigned)*dsn;
	const int* vehicles_division = vd + vehicle_permutation * dv->vehicles_number;
	const int cutoff = dv->open_time + (dv->close_time - dv->open_time) / 2;
	double current_cost = 0.0f;
	double max_time = dv->open_time;
	for (int i = 0, n = 0; i < dv->vehicles_number; ++i)
	{
		double current_time = dv->open_time;
		int current_capacity = dv->vehicle_capacity;
		int index = 0;
		for (int j = 0; j < vehicles_division[i]; ++j, ++n)
		{
			if (ds[depot_permutation[n]].time_available <= cutoff)
			{
				if (current_time < (double)ds[depot_permutation[n]].time_available)
				{
					//wait until demand is available
					current_time = (double)ds[depot_permutation[n]].time_available;
				}
			}
			//unload cargo
			current_capacity += ds[depot_permutation[n]].demand;

			if (current_capacity < 0)
			{
				current_cost += costs[index];
				current_time += costs[index];
				current_capacity = dv->vehicle_capacity + ds[depot_permutation[n]].demand;
				index = 0;
			}

			//travel to client
			current_cost += costs[(*dsn) * index + depot_permutation[n] + 1];
			current_time += costs[(*dsn) * index + depot_permutation[n] + 1];

			current_time += ds[depot_permutation[n]].duration_time;

			index = depot_permutation[n] + 1;

			if (current_cost >= *minimum || current_time > (double)dv->close_time)
			{
				results[premutation_number * (*vpc) + vehicle_permutation] = 9999;// CUDART_INF_F;
				return;
			}
		}
		//go back to my depot
		current_cost += costs[index];
		current_time += costs[index];

		if (max_time < current_time)
		{
			max_time = current_time;
			if (max_time > (double)dv->close_time)
			{
				results[premutation_number * (*vpc) + vehicle_permutation] = 9999;// CUDART_INF_F;
				return;
			}
		}
	}
	results[premutation_number * (*vpc) + vehicle_permutation] = current_cost;
}

double LaunchKernel(const ullint perm_start, const int kernel_size, const int vehicle_permutations_count, const int dsn, const int perm_size, const int results_count,
	const bool minimum_changed, const double minimum, int* permuts, int* cpermuts, double* cresult, Depot* cds, DepotVehicle* cdv,
	int* cdsn, int* cvd, int* cvpc, double* ccosts, double* cminimum, const thrust::device_ptr<double>& cresults_wrapper, ullint& perm_number_min, int& vehicle_perm_number_min)
{
	ullint perm_end = perm_start + kernel_size;
	cudaError_t cudaStatus;
	GetPermuts(permuts, perm_start, perm_end, dsn);

	if (minimum_changed)
	{
		cudaStatus = cudaMemcpy(cminimum, &minimum, sizeof(double), cudaMemcpyHostToDevice);
		if (cudaStatus != cudaSuccess) {
			fprintf(stderr, "cudaMemcpy failed!");
		}
	}

	cudaStatus = cudaMemcpy(cpermuts, permuts, perm_size, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	dim3 blocks(vehicle_permutations_count);
	dim3 threadsPerBlock(kernel_size);

	dvrpKernel << <blocks, threadsPerBlock >> >(cpermuts, cds, cdv, cdsn, cvd, cvpc, cminimum, ccosts, cresult);
	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}

	thrust::device_ptr<double> pos = thrust::min_element(cresults_wrapper, cresults_wrapper + results_count);
	auto distance = thrust::distance(cresults_wrapper, pos);

	int relative_perm_number_min = distance / vehicle_permutations_count;
	vehicle_perm_number_min = distance - relative_perm_number_min*vehicle_permutations_count;
	perm_number_min = perm_start + relative_perm_number_min;
	return cresults_wrapper[distance];
}

double GetCost(int sx, int sy, int ex, int ey)
{
	int dx = sx - ex;
	int dy = sy - ey;
	return sqrt((double)(dx*dx + dy*dy));
}

double* GetCosts(const DepotVehicle dv, const Depot* ds, int dns)
{
	double* costs = (double*)malloc((dns + 1) * (dns + 1) * sizeof(double));
	costs[0] = 0;
	for (int i = 0; i < dns; i++)
	{
		auto cost = GetCost(dv.x, dv.y, ds[i].x, ds[i].y);;
		costs[i + 1] = cost;
		costs[(i + 1) * dns] = cost;
	}
	for (int i = 0; i < dns; i++)
	{
		for (int j = 0; j < dns; j++)
		{
			costs[(i + 1) * dns + j + 1] = GetCost(ds[i].x, ds[i].y, ds[j].x, ds[j].y);
		}
	}
	return costs;
}

// unique - e.g. 1 2 3, non-unique - e.g. 1 1 ... 1 and 0 10 0 ... 0
void DivideIntoUniqueAndNonUniqueDistributions(vector<vector<int>> vp, vector<vector<int>>& unique, vector<vector<int>>& nonUnique)
{
	unique.clear();
	nonUnique.clear();
	for (auto i : vp)
	{
		bool isUnique = false;
		bool numberFound = false;
		for (auto j : i)
		{
			if (j != 0 && !numberFound)
			{
				numberFound = true;
				continue;
			}
			if (j != 0)
			{
				isUnique = true;
				break;
			}
		}
		if (isUnique)
			unique.push_back(i);
		else
			nonUnique.push_back(i);
	}
}

typedef struct SubProblem
{
	ullint perms_start;
	ullint perms_count;
	int non_unique;

} SubProblem;

ExportDLL void FreeMemory(void* subProblem)
{
	//free(subProblem);
}

ExportDLL SubProblem* Divide(DepotVehicle dv, Depot* ds, int dsn, int numberOfNodes, int& subProblemCount)
{
	ullint perms_count = Factorial(dsn);
	
	ullint perms_per_node = perms_count / numberOfNodes;
	ullint leftover_nodes = perms_count - perms_per_node * numberOfNodes;

	SubProblem* result;

	if (leftover_nodes == 0)
	{
		result = (SubProblem*)malloc((numberOfNodes) * sizeof(SubProblem));
		subProblemCount = numberOfNodes;
	}
	else
	{
		result = (SubProblem*)malloc((numberOfNodes + 1) * sizeof(SubProblem));
		subProblemCount = numberOfNodes + 1;
	}


	for (int i = 0; i < numberOfNodes; ++i)
	{
		result[i].perms_count = perms_per_node;
		result[i].perms_start = i*perms_per_node;
		result[i].non_unique = 0;
	}

	if (leftover_nodes != 0)
	{
		result[numberOfNodes].perms_count = leftover_nodes;
		result[numberOfNodes].perms_start = perms_per_node * numberOfNodes;
		result[numberOfNodes].non_unique = 0;
	}

	return result;
}

ExportDLL void GetPermutations(DepotVehicle dv, int dsn, int vehicle_permutation_number, ullint permutation_number, int* vehicle_permutation, int* permutation)
{
	get_permutation(permutation_number, dsn, permutation);

	const int parts = dv.vehicles_number;
	vector<vector<int>> vehicles_permutations_vector = NumberIntoParts(dsn, parts);
	for (int i = 0; i < vehicles_permutations_vector[vehicle_permutation_number].size(); i++)
	{
		vehicle_permutation[i] = vehicles_permutations_vector[vehicle_permutation_number][i];
	}
}

ExportDLL double Solve(DepotVehicle dv, Depot* ds, int dsn, void* subProblemData, ullint& perm_number_min, int& vehicle_perm_number_min)
{
	SubProblem subProblem = *(SubProblem*)(subProblemData);
	const int KERNEL_SIZE = 1024;
	const int parts = dv.vehicles_number;
	vector<vector<int>> vehicles_permutations_vector = NumberIntoParts(dsn, parts);
	int* vehicles_permutations;
	vector<vector<int>> unique_vehicles_permutations_vector, non_unique_vehicles_permutations_vector;
	DivideIntoUniqueAndNonUniqueDistributions(vehicles_permutations_vector, unique_vehicles_permutations_vector, non_unique_vehicles_permutations_vector);
	vehicles_permutations = ConvertToIntArray(vehicles_permutations_vector, parts);
	double* costs = GetCosts(dv, ds, dsn);

	int costs_size = (dsn + 1) * (dsn + 1) * sizeof(double);

	int vehicles_permutations_count = vehicles_permutations_vector.size();
	int vehicles_permutations_size = vehicles_permutations_count  * parts * sizeof(int);
	int perms_size = KERNEL_SIZE * dsn * sizeof(int);
	int results_size = vehicles_permutations_count  * KERNEL_SIZE * sizeof(double);
	int results_count = vehicles_permutations_count * KERNEL_SIZE - 1;
	int* perms = (int*)malloc(perms_size);
	int perms_count = subProblem.perms_count;

	Depot* cds;
	DepotVehicle* cdv;
	int* cdsn;
	int* cvd;
	int* cvpc;
	int* cperms;
	double* cresults;
	double* ccosts;
	double* cminimum;

	cudaError_t cudaStatus;

	cudaStatus = cudaMalloc((void**)&cminimum, sizeof(double));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMalloc((void**)&ccosts, costs_size);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMemcpy(ccosts, costs, costs_size, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	cudaStatus = cudaMalloc((void**)&cds, dsn * sizeof(Depot));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMemcpy(cds, ds, dsn * sizeof(Depot), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	cudaStatus = cudaMalloc((void**)&cdv, sizeof(DepotVehicle));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMemcpy(cdv, &dv, sizeof(DepotVehicle), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	cudaStatus = cudaMalloc((void**)&cdsn, sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMemcpy(cdsn, &dsn, sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	cudaStatus = cudaMalloc((void**)&cvpc, sizeof(int));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMalloc((void**)&cperms, perms_size);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMalloc((void**)&cresults, results_size);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	bool minimum_changed = true;
	double minimum = numeric_limits<double>::max();
	ullint perm_number_min_tmp;
	int vehicle_perm_number_min_tmp;
	double res;
	thrust::device_ptr<double> cresults_wrapper = thrust::device_pointer_cast(cresults);

	ullint kernel_number = ceil((double)((double)perms_count / (double)KERNEL_SIZE));

	cudaStatus = cudaMemcpy(cvpc, &vehicles_permutations_count, sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	cudaStatus = cudaMalloc((void**)&cvd, vehicles_permutations_size);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMemcpy(cvd, vehicles_permutations, vehicles_permutations_size, cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	//not full

	thrust::fill(cresults_wrapper, cresults_wrapper + results_count, numeric_limits<double>::max());
	res = LaunchKernel((kernel_number - 1) * KERNEL_SIZE + subProblem.perms_start, perms_count - (kernel_number - 1) * KERNEL_SIZE, vehicles_permutations_count, dsn, perms_size, results_count, minimum_changed, minimum, perms,
		cperms, cresults, cds, cdv, cdsn, cvd, cvpc, ccosts, cminimum, cresults_wrapper, perm_number_min_tmp, vehicle_perm_number_min_tmp);
	if (res < minimum)
	{
		minimum_changed = true;
		minimum = res;
		perm_number_min = perm_number_min_tmp;
		vehicle_perm_number_min = vehicle_perm_number_min_tmp;
	}
	else
	{
		minimum_changed = false;
	}

	//full

	if (kernel_number > 1)
	{
		for (ullint i = kernel_number - 2; i > 0; --i)
		{
			res = LaunchKernel(i*KERNEL_SIZE + subProblem.perms_start, KERNEL_SIZE, vehicles_permutations_count, dsn, perms_size, results_count, minimum_changed, minimum, perms,
				cperms, cresults, cds, cdv, cdsn, cvd, cvpc, ccosts, cminimum, cresults_wrapper, perm_number_min_tmp, vehicle_perm_number_min_tmp);
			if (res < minimum)
			{
				minimum_changed = true;
				minimum = res;
				perm_number_min = perm_number_min_tmp;
				vehicle_perm_number_min = vehicle_perm_number_min_tmp;
			}
			else
			{
				minimum_changed = false;
			}
		}
	}

	free(perms);

	cudaStatus = cudaFree(cds);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cds cudaFree failed!\n");
	}

	cudaStatus = cudaFree(cdsn);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cdsn cudaFree failed!\n");
	}

	cudaStatus = cudaFree(cdv);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cdv cudaFree failed!\n");
	}

	cudaStatus = cudaFree(cvd);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cvd cudaFree failed!\n");
	}

	cudaStatus = cudaFree(cvpc);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, " cvpccvpccudaFree failed!\n");
	}

	cudaStatus = cudaFree(cperms);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cperms cudaFree failed!\n");
	}

	cudaStatus = cudaFree(cresults);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cresults cudaFree failed!\n");
	}

	cudaStatus = cudaFree(ccosts);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "ccosts cudaFree failed!\n");
	}

	cudaStatus = cudaFree(cminimum);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cminimum cudaFree failed!\n");
	}

	cudaStatus =  cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize failed!\n");
	}

	return minimum;
}