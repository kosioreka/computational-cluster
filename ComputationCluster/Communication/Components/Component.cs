﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Communication
{
    public class Component
    {
        private ulong _id;
        private ComponentType _type;
        private string[] _solvableProblems;
        private byte _parallelThreads;
        private StatusThread[] _threads;

        public StatusThread[] Threads
        {
            get { return _threads; }
            set { _threads = value; }
        }

        public byte ParallelThreads
        {
            get { return _parallelThreads; }
            set
            {
                _parallelThreads = value;
                Threads = new StatusThread[value];
            }
        }

        public string[] SolvableProblems
        {
            get { return _solvableProblems; }
            set { _solvableProblems = value; }
        }

        public ulong ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public ComponentType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public CancellationToken Token
        {
            get; set;
        }

        public CancellationTokenSource TokenSource
        {
            get; set;
        }

        public Task TimeoutDeregister
        {
            get; set;
        }

        public enum ComponentType
        {
            TaskManager,
            ComputationalNode,
            CommunicationServer,
            ComputationalClient
        }

        public static ComponentType mapToCType(RegisterType rt)
        {
            switch (rt)
            {
                case RegisterType.CommunicationServer:
                    return ComponentType.CommunicationServer;
                case RegisterType.ComputationalNode:
                    return ComponentType.ComputationalNode;
                case RegisterType.TaskManager:
                    return ComponentType.TaskManager;
                default:
                    throw new NotSupportedException();
            }
        }
    }
}