﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Communication
{
    public class Problem
    {
        private byte[] _data;
        private ulong _ID;
        private string _problemType;//to ProblemType
        private ulong _solvingTimeout;
        private ProblemStatus _status;
        private List<PartialProblem> _partialProblems = new List<PartialProblem>();
        private List<PartialSolution> _partialSolutions = new List<PartialSolution>();
        private List<PartialSolution> _bestSolutions = new List<PartialSolution>();
        private ulong _tmID;

        public ulong TMID
        {
            get { return _tmID; }
            set { _tmID = value; }
        }

        public List<PartialSolution> BestSolution
        {
            get { return _bestSolutions; }
            set { _bestSolutions = value; }
        }

        public List<PartialSolution> PartialSolutions
        {
            get { return _partialSolutions; }
            set { _partialSolutions = value; }
        }

        public List<PartialProblem> PartialProblems
        {
            get { return _partialProblems; }
            set { _partialProblems = value; }
        }

        public ulong SolvingTimeout
        {
            get { return _solvingTimeout; }
            set { _solvingTimeout = value; }
        }

        public bool SolvingTimeoutSpecified
        {
            get { return SolvingTimeout != null && SolvingTimeout != 0; }
        }

        public string ProblemType
        {
            get { return _problemType; }
            set { _problemType = value; }
        }

        public ulong ID
        {
            get { return _ID; }
            set { _ID = value; }
        }

        public byte[] Data {
            get { return _data; }
            set { _data = value; }
        }
        public ProblemStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public void UpdateProblemStatus()
        {
            if (this.PartialProblems == null || !this.PartialProblems.Any())
            {
                this.Status = ProblemStatus.ToDivide;
                return;
            }
            if (this.PartialProblems.Exists(p => p.Status == ProblemStatus.ToCompute))
            {
                if (this.PartialProblems.All(p => p.Status == ProblemStatus.ToCompute))
                    this.Status = ProblemStatus.ToCompute;
                else
                    this.Status = ProblemStatus.Dividing;
                return;
            }
            if (this.PartialProblems.Exists(p => p.Status == ProblemStatus.Computing))
            {
                this.Status = ProblemStatus.Computing;
                return;
            }
            if (this.PartialProblems.All(p => p.Status == ProblemStatus.Done))
            {
                if (this.PartialSolutions.All(s => s.Status == ProblemStatus.GetFinalSolution))
                {
                    this.Status = ProblemStatus.GetFinalSolution;
                    return;
                }
                if (this.PartialSolutions.Any(s => s.Status == ProblemStatus.GettingFinalSolution))
                {
                    this.Status = ProblemStatus.GettingFinalSolution;
                    return;
                }
                if (this.PartialSolutions.All(s => s.Status == ProblemStatus.Done))
                {
                    this.Status = ProblemStatus.ReadyToSend;
                    return;
                }
            }
        }
    }
    public class PartialProblem
    {
        private ulong _taskId;
        private byte[] _data;
        private ulong _nodeId;
        private ProblemStatus _status;

        public ProblemStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public ulong TaskId
        {
            get { return _taskId; }
            set { _taskId = value; }
        }
        public byte[] Data
        {
            get { return _data; }
            set{ _data = value; }
        }
        public ulong NodeId
        {
            get { return _nodeId; }
            set { _nodeId = value; }
        }
    }

    public class PartialSolution
    {
        private ulong _taskId;
        private bool _timeoutOccurd;
        private byte[] _data;
        private ProblemStatus _status;

        public ProblemStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        //cpomputations time
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public ulong TaskId
        {
            get { return _taskId; }
            set { _taskId = value; }
        }
        public bool TimeoutOccurd
        {
            get { return _timeoutOccurd; }
            set
            {
                _timeoutOccurd = value;
            }
        }

    }

    public enum ProblemStatus
    {
        ToDivide,                  //ParPr: null, ParSol: null
        Dividing,                  //ParPr: Any: ToCompute, ParSol: null
        ToCompute,                //ParPr: All: ToCompute, ParSol: null
        Computing,                //ParPr: Any: Computing, ParSol: (Any: GetFinalSolution)
        GetFinalSolution,         //ParPr: All: Done, ParSol: All: GetFinalSolution
        GettingFinalSolution,     //ParPr: All: Done, ParSol: Any: GettingFinalSolution
        ReadyToSend,           //ParPr: All: Done, ParSol: All: Done
        Sent,
        Done                      //used for ParPr and ParSol when all components computed!
    }
}
