﻿using System;

namespace Communication
{
    public interface IXMLSerializator
    {
        object DeserializeFromXML(string xmlString, Type objectType);
        string SerializeToXML(object objectToSerialize, Type objectType);
    }
}