﻿namespace Communication
{
    public interface IServersHierarchyManager
    {
        void AddBackupServer(params ConnectionData[] servers);
        /// <summary>
        /// Updates current server, returns true if backup is available and false if not.
        /// </summary>
        /// <returns></returns>
        bool CurrentServerNotResponding();
        ConnectionData GetCurrentServerData();
    }
}