﻿using Communication.Data;
using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace Communication
{
    using MessagesCallbacksCollection = Dictionary<Type, IncomingMessageCallback>;

    public class ConnectionManager : CommunicationBaseClass, IConnectionManager, IDisposable
    {
        private bool _isListening = false;
        private Socket _socket;
        private IXMLSerializator _xmlSerializator;

        private MessagesCallbacksCollection _incomingMessagesCallbacks;
        public MessagesCallbacksCollection IncomingMessagesCallbacks
        {
            get
            {
                return _incomingMessagesCallbacks;
            }
            set
            {
                if (_isListening)
                    throw new InvalidOperationException(SettingsData.ALREADY_LISTENING);
                _incomingMessagesCallbacks = value;
            }
        }

        private int _port;
        public int Port
        {
            get
            {
                return _port;
            }
            set
            {
                if (_isListening)
                    throw new InvalidOperationException(SettingsData.ALREADY_LISTENING);
                _port = value;
            }
        }

        public ConnectionManager(IXMLSerializator xmlSerializator)
        {
            _xmlSerializator = xmlSerializator;
            _socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Dispose()
        {
            StopListening();
        }

        public void StartListening()
        {
            _isListening = true;
            var localEP = new IPEndPoint(IPAddress.Any, _port);
            try
            {
                _socket.Bind(localEP);
                _socket.Listen(SettingsData.CONNECTION_QUEUE_MAX_LENGTH);
                _socket.BeginAccept(new AsyncCallback(AcceptClient), _socket);
            }
            catch (Exception e)
            {
                //TODO
                throw;
            }
        }

        public void StopListening()
        {
            if (!_isListening)
                return;
            _isListening = false;
            _socket.Close();
        }

        private void AcceptClient(IAsyncResult ar)
        {
            if (!_isListening)
                return;
            var socket = ar.AsyncState as Socket;
            Socket handler;
            try
            {
                _socket.BeginAccept(new AsyncCallback(AcceptClient), _socket);
                handler = socket.EndAccept(ar);
            }
            catch (Exception e)
            {
                //TODO
                throw;
            }
            var stateObject = new StateObject();
            stateObject.WorkSocket = handler;
            try
            {
                handler.BeginReceive(stateObject.Buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), stateObject);
            }
            catch (Exception e)
            {
                //TODO
                throw;
            }
            ResetTimeout(stateObject);
        }

        protected override void ReadCallback(IAsyncResult ar)
        {
            var stateObject = ar.AsyncState as StateObject;
            if (stateObject.OutOfTime)
                return;
            var handler = stateObject.WorkSocket;
            int bytesRead;
            try
            {
                bytesRead = handler.EndReceive(ar);
            }
            catch (Exception e)
            {
                //TODO
                throw;
            }
            if (bytesRead > 0)
            {
                var receivedString = SettingsData.MESSAGES_ENCODING.GetString(
                    stateObject.Buffer, 0, bytesRead);
                stateObject.StringBuilder.Append(receivedString);
                if (bytesRead >= StateObject.BufferSize)
                {
                    try
                    {
                        handler.BeginReceive(stateObject.Buffer, 0, StateObject.BufferSize, 0,
                             new AsyncCallback(ReadCallback), stateObject);
                    }
                    catch (Exception e)
                    {
                        //TODO
                        throw;
                    }
                    ResetTimeout(stateObject);
                    return;
                }
            }
            handler.Shutdown(SocketShutdown.Receive);
            ReadFinished(stateObject);
        }

        protected override void ReadFinished(StateObject stateObject)
        {
            var message = stateObject.StringBuilder.ToString();
            var handler = stateObject.WorkSocket;
            var responses = InvokeValidCallbackAndGetResponseMessages(message, stateObject);
            Send(responses, stateObject);
        }

        private void Send(string[] responses, StateObject stateObject)
        {
            var delimeterChar = new char[] { (char)SettingsData.MESSAGES_DELIMETER };
            var delimeterBytes = Encoding.UTF8.GetBytes(delimeterChar);
            IEnumerable<byte> bytesToSend = new List<byte>();
            var firstResponse = true;
            foreach (var response in responses)
            {
                var byteResponse = Encoding.UTF8.GetBytes(response);
                if (!firstResponse)
                {
                    bytesToSend = bytesToSend.Concat(delimeterBytes.ToList());
                }
                firstResponse = false;
                bytesToSend = bytesToSend.Concat(byteResponse.ToList());
            }
            var data = bytesToSend.ToArray();
            try
            {
                stateObject.WorkSocket.BeginSend(data, 0, data.Length, 0,
                    new AsyncCallback(SendCallback), stateObject);
            }
            catch (Exception e)
            {
                //TODO
                throw;
            }
            ResetTimeout(stateObject);
        }

        protected override bool TryHandleResponseMessage(string message)
        {
            throw new NotImplementedException();
        }

        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                var stateObject = ar.AsyncState as StateObject;
                var handler = stateObject.WorkSocket;
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
            }
            catch (Exception e)
            {
                //TODO
                throw;
            }
        }

        private string[] InvokeValidCallbackAndGetResponseMessages(string stringMessage, StateObject stateObject)
        {
            if (_incomingMessagesCallbacks == null)
                throw new ArgumentException(SettingsData.NO_VALID_CALLBACK);
            foreach (var element in _incomingMessagesCallbacks)
            {
                var callbackIn = element.Key;
                var deserilizedObject = _xmlSerializator.DeserializeFromXML(stringMessage, callbackIn);
                if (deserilizedObject != null)
                {
                    //Dirty, dirty hack ¯\_(ツ)_ /¯
                    if (callbackIn == typeof(Register))
                        (deserilizedObject as Register).ConnectionParameters = 
                            stateObject.WorkSocket.RemoteEndPoint as IPEndPoint;
                    var responses = element.Value.Invoke(deserilizedObject);
                    var responseMessagesList = new List<string>();
                    foreach (var response in responses)
                    {
                        var responseMessage = _xmlSerializator.SerializeToXML(response.Item1,
                                                                              response.Item2);
                        responseMessagesList.Add(responseMessage);
                    }
                    return responseMessagesList.ToArray();
                }
            }
            throw new ArgumentException(SettingsData.NO_VALID_CALLBACK);
        }
    }
}