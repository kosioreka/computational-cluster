﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Communication
{
    using ResponseCallbacks = List<Tuple<Type, ResponseCallback>>; //<Type, ResponseCallback>;
    public delegate void ResponseCallback(object response);
    public interface IMessageSenderManager
    {
        /// <summary>
        /// Sends given message to address specified in connectionData. 
        /// For each response received it calls approperiate callback.
        /// It is guaranteed that the object in the argument will be the type of Type.
        /// </summary>
        /// <param name="connectionData"></param>
        /// <param name="message"></param>
        /// <param name="responsesCallbacks">Dictionary with Type of the response as the key and callback as a value.</param>
        /// <returns></returns>
        Task<bool> SendMessage<T>(T message, ResponseCallbacks responsesCallbacks);
    }
}