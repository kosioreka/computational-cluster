﻿using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Communication
{
    class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding => Encoding.UTF8;
    }
    
    /// <summary>
    /// Serializes and deserializes to and from utf-8 encoded XMLs to and from <T> objects
    /// </summary>
    public class XMLSerializator : IXMLSerializator
    {
        public string SerializeToXML(object objectToSerialize, Type objectType)
        {
            var serializer = new XmlSerializer(objectType);
            using (var stringWriter = new Utf8StringWriter())
            {
                serializer.Serialize(stringWriter, objectToSerialize);
                return stringWriter.ToString();
            }
        }

        public object DeserializeFromXML(string xmlString, Type objectType)
        {
            var UTF8bytes = StringToUTF8ByteArray(xmlString);
            using (var memoryStream = new MemoryStream(UTF8bytes))
            {
                using (var xmlReader = XmlReader.Create(memoryStream))
                {
                    var xmlSerializer = new XmlSerializer(objectType);
                    try
                    {
                        if (!xmlSerializer.CanDeserialize(xmlReader))
                            return null;
                    }
                    catch(XmlException)
                    {
                        return null;
                    }
                    return xmlSerializer.Deserialize(xmlReader);
                }
            }
        }

        private byte[] StringToUTF8ByteArray(string xmlString)
        {
            var encoding = new UTF8Encoding();
            return encoding.GetBytes(xmlString);
        }
    }
}