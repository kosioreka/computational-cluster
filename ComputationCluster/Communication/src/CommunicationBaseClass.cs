﻿using Communication.Data;
using System;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Communication
{
    public abstract class CommunicationBaseClass
    {
        protected Task _timeoutTask;
        protected TaskCompletionSource<bool> _timeoutResetTaskSource;

        protected virtual void ReadCallback(IAsyncResult ar)
        {
            var stateObject = ar.AsyncState as StateObject;
            if (stateObject.OutOfTime)
                return;
            var handler = stateObject.WorkSocket;
            int bytesRead;
            try
            {
                bytesRead = handler.EndReceive(ar);
            }
            catch (SocketException)
            {
                TryRedirectMessageToBackup(stateObject);
                return;
            }
            if (bytesRead > 0)
            {
                var receivedString = SettingsData.MESSAGES_ENCODING.GetString(
                    stateObject.Buffer, 0, bytesRead);
                stateObject.StringBuilder.Append(receivedString);
                try
                {
                    handler.BeginReceive(stateObject.Buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallback), stateObject);
                }
                catch (SocketException)
                {
                    TryRedirectMessageToBackup(stateObject);
                    return;
                }
                ResetTimeout(stateObject);
            }
            else
            {
                if (TryHandleResponseMessage(stateObject.StringBuilder.ToString()))
                {
                    try
                    {
                        handler.Shutdown(SocketShutdown.Both);
                        handler.Close();
                    }
                    catch (SocketException)
                    {
                        TryRedirectMessageToBackup(stateObject);
                        return;
                    }
                    ReadFinished(stateObject);
                }
                else
                    handler.BeginReceive(stateObject.Buffer, 0, StateObject.BufferSize, 0,
                        new AsyncCallback(ReadCallback), stateObject);
            }
        }

        protected virtual void TryRedirectMessageToBackup(StateObject oldStateObject)
        {
        }

        protected abstract bool TryHandleResponseMessage(string message);

        protected void ResetTimeout(StateObject stateObject)
        {
            if (_timeoutResetTaskSource != null && 
                _timeoutResetTaskSource.Task.Status != TaskStatus.RanToCompletion)
                _timeoutResetTaskSource.SetResult(true);
            _timeoutResetTaskSource = new TaskCompletionSource<bool>();
            stateObject.OutOfTime = false;
            _timeoutTask = Timeout(stateObject);
        }

        protected virtual async Task Timeout(StateObject stateObject)
        {
            var delayTask = Task.Delay(SettingsData.CONNECTION_TIMEOUT);
            var taskRace = await Task.WhenAny(delayTask, _timeoutResetTaskSource.Task);
            if (taskRace != delayTask)
                return;
            //stateObject.OutOfTime = true;
            //Todo: Timeouts not needed probably
        }

        protected abstract void ReadFinished(StateObject stateObject);
    }

    public class StateObject
    {
        public Socket WorkSocket { get; set; }
        public const int BufferSize = SettingsData.READ_BYTES_COUNT;
        private byte[] _buffer = new byte[BufferSize];
        private StringBuilder _stringBuilder = new StringBuilder();

        private bool _outOfTime = false;
        public byte[] Buffer
        {
            get { return _buffer; }
            set { _buffer = value; }
        }

        public StringBuilder StringBuilder
        {
            get { return _stringBuilder; }
            set { _stringBuilder = value; }
        }

        public bool OutOfTime
        {
            get { return _outOfTime; }
            set { _outOfTime = value; }
        }

        public bool DeadSession { get; set; } = false;
    }
}