﻿using System;

namespace Communication
{
    public class ServersHierarchyManagerForServer : IServersHierarchyManager
    {
        private ConnectionData _masterServerData;
        private Action _masterServetNotResponding;

        public ServersHierarchyManagerForServer(ConnectionData masterServer, Action masterServerNotResponding)
        {
            _masterServetNotResponding = masterServerNotResponding;
            _masterServerData = masterServer;
        }

        public void AddBackupServer(params ConnectionData[] servers)
        {
        }

        public bool CurrentServerNotResponding()
        {
            _masterServetNotResponding.Invoke();
            return false;
        }

        public ConnectionData GetCurrentServerData()
        {
            return _masterServerData;
        }
    }
}