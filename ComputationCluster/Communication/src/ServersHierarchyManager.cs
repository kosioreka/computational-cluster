﻿using System;
using System.Collections.Generic;

namespace Communication
{
    public class ServersHierarchyManager : IServersHierarchyManager
    {
        private List<ConnectionData> _serversHierarchy = new List<ConnectionData>();
        private ConnectionData _masterServerData;
        private bool _masterWorking = true;
        private int _currentIndex = 0;
        private object _lock = new object();

        public ServersHierarchyManager(ConnectionData masterServer)
        {
            _masterServerData = masterServer;
        }

        public ConnectionData GetCurrentServerData()
        {
            lock (_lock)
            {
                if (_masterWorking)
                    return _masterServerData;
                else
                    return _serversHierarchy[_currentIndex];
            }
        }

        public void AddBackupServer(params ConnectionData[] servers)
        {
            lock (_lock)
            {
                _currentIndex = 0;
                _serversHierarchy.Clear();
                _serversHierarchy.AddRange(servers);
            }
        }

        public bool CurrentServerNotResponding()
        {
            lock (_lock)
            {
                if (_masterWorking)
                    _masterWorking = false;
                else
                    _currentIndex++;
                if (_serversHierarchy.Count <= _currentIndex)
                    return false;
                return true;
            }
        }
    }
}