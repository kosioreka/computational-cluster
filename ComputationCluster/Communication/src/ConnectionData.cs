﻿using System.Net;

namespace Communication
{
    public class ConnectionData
    {
        public ConnectionData(string ipAddress, int port)
        {
            IPAddress = IPAddress.Parse(ipAddress);
            Port = port;
        }

        public IPAddress IPAddress { get; set; }
        public int Port { get; set; }
    }
}