﻿using System;
using System.Text;

namespace Communication.Data
{
    static class SettingsData
    {
        public const int READ_BYTES_COUNT = 256;
        public const int MESSAGES_DELIMETER = 23;
        public static Encoding MESSAGES_ENCODING = Encoding.UTF8;
        public static TimeSpan CONNECTION_TIMEOUT = TimeSpan.FromSeconds(1);
        public const string NO_VALID_CALLBACK = "No valid callback!";
        public const string ALREADY_LISTENING = "Already listening on some port!";
        public const string ALREADY_SENDING_MESSAGE = "Already sending message!";
        public const int CONNECTION_QUEUE_MAX_LENGTH = 100;
    }
}