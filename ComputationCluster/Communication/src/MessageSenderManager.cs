﻿using Communication.Data;
using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Net;
using System.Threading;

namespace Communication
{
    using ResponseCallbacks = List<Tuple<Type, ResponseCallback>>;

    public class MessageSenderManager : CommunicationBaseClass, IMessageSenderManager, IDisposable
    {
        private ResponseCallbacks _responseCallbacks;
        private string _message;
        private Socket _client;
        private IXMLSerializator _xmlSerializator;
        private TaskCompletionSource<bool> _sendMessageTaskCompletion = new TaskCompletionSource<bool>();
        private bool _alreadySending = false;
        private IServersHierarchyManager _serversManager;
        private ConnectionData _localConnectionData;
        private Task _sendingMessageToBackupServerTask;

        public MessageSenderManager(IXMLSerializator xmlSerializator, 
            IServersHierarchyManager serversHierarchyManager, ConnectionData localConnectionData = null)
        {
            _xmlSerializator = xmlSerializator;
            _serversManager = serversHierarchyManager;
            if (localConnectionData != null)
                _localConnectionData = localConnectionData;
        }

        public void Dispose()
        {
            _sendMessageTaskCompletion.SetResult(true);
        }

        protected override async Task Timeout(StateObject stateObject)
        {
            var delayTask = Task.Delay(SettingsData.CONNECTION_TIMEOUT);
            var taskRace = await Task.WhenAny(delayTask, _timeoutResetTaskSource.Task);
            if (taskRace != delayTask)
                return;
            //stateObject.OutOfTime = true;
            //TryRedirectMessageToBackup(stateObject);
        }

        protected override void TryRedirectMessageToBackup(StateObject oldStateObject)
        {
            oldStateObject.DeadSession = true;
            if (_serversManager.CurrentServerNotResponding())
            {
                 if (_sendingMessageToBackupServerTask == null ||
                    _sendingMessageToBackupServerTask.Status != TaskStatus.RanToCompletion)
                {
                    _sendingMessageToBackupServerTask = SendMessage();
                }
            }
            else
            {
                _client.Close();
                _sendMessageTaskCompletion.SetResult(false);
            }
        }

        public async Task<bool> SendMessage<T>(T message, ResponseCallbacks responsesCallbacks)
        {
            if (_alreadySending)
                throw new InvalidOperationException(SettingsData.ALREADY_SENDING_MESSAGE);
            _alreadySending = true;
            _client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            if (_localConnectionData != null)
            {
                var localEndPoint = new IPEndPoint(_localConnectionData.IPAddress, _localConnectionData.Port);
                try
                {
                    _client.Bind(localEndPoint);
                }
                catch (SocketException)
                {
                    return false;
                }
            }
            _message = _xmlSerializator.SerializeToXML(message, typeof(T));
            _responseCallbacks = responsesCallbacks;
            AddBackupCallbacksToResponses();
            return await SendMessage();
        }

        private async Task<bool> SendMessage()
        {
            var stateObject = new StateObject();
            stateObject.WorkSocket = _client;
            var connectionData = _serversManager.GetCurrentServerData();
            var remoteEP = new IPEndPoint(connectionData.IPAddress, connectionData.Port);
            try
            {
                _client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), stateObject);
            }
            catch (SocketException)
            {
                TryRedirectMessageToBackup(stateObject);
            }
            var result = await _sendMessageTaskCompletion.Task;
            _alreadySending = false;
            return result;
        }

        private void AddBackupCallbacksToResponses()
        {
            var backupServersCallback = new ResponseCallback(((noOperation) =>
            {
                var noOperationObject = noOperation as BackupServers;
                if (noOperationObject == null || noOperationObject.BackupCommunicationServers == null)
                    return;
                var connectionDataList = new List<ConnectionData>();
                foreach (var server in noOperationObject.BackupCommunicationServers)
                {
                    var connectionData = new ConnectionData(server.address, server.port);
                    connectionDataList.Add(connectionData);
                }
                _serversManager.AddBackupServer(connectionDataList.ToArray());
            }));
            _responseCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers), backupServersCallback));
        }

        private void ConnectCallback(IAsyncResult ar)
        {
            var stateObject = ar.AsyncState as StateObject;
            if (stateObject.OutOfTime)
                return;
            ResetTimeout(stateObject);
            try
            {
                _client.EndConnect(ar);
            }
            catch (SocketException)
            {
                TryRedirectMessageToBackup(stateObject);
                return;
            }
            Send(stateObject);
        }

        private void Send(StateObject stateObject)
        {
            ResetTimeout(stateObject);
            var bytes = SettingsData.MESSAGES_ENCODING.GetBytes(_message);
            try
            {
                _client.BeginSend(bytes, 0, bytes.Length, 0, new AsyncCallback(SendCallback), stateObject);
            }
            catch (SocketException)
            {
                TryRedirectMessageToBackup(stateObject);
                return;
            }
        }

        private void SendCallback(IAsyncResult ar)
        {
            var stateObject = ar.AsyncState as StateObject;
            if (stateObject.OutOfTime)
                return;
            ResetTimeout(stateObject);
            Read(stateObject);
        }

        private void Read(StateObject stateObject)
        {
            ResetTimeout(stateObject);
            try
            {
                _client.BeginReceive(stateObject.Buffer, 0, StateObject.BufferSize, 0,
                    new AsyncCallback(ReadCallback), stateObject);
            }
            catch (SocketException)
            {
                TryRedirectMessageToBackup(stateObject);
                return;
            }
        }

        protected override void ReadFinished(StateObject stateObject)
        {
            ResetTimeout(stateObject);
            var message = stateObject.StringBuilder.ToString();
            while (message.Length > 0)
                message = HandleResponseMessage(message);
            _sendMessageTaskCompletion.SetResult(true);
        }

        protected override bool TryHandleResponseMessage(string response)
        {
            string overheadString;
            var message = GetSingleXMLMessage(response, out overheadString);
            foreach (var element in _responseCallbacks)
            {
                var deserialisedObject = _xmlSerializator.DeserializeFromXML(message, element.Item1);
                if (deserialisedObject != null)
                {
                    return true;
                }
            }
            return false;
        }

        private string HandleResponseMessage(string response)
        {
            string overheadString;
            var message = GetSingleXMLMessage(response, out overheadString);
            var delimiterIndex = response.IndexOf((char)SettingsData.MESSAGES_DELIMETER);
            InvokeValidCallback(message);
            return overheadString;
        }

        private string GetSingleXMLMessage(string response, out string overheadString)
        {
            var delimiterIndex = response.IndexOf((char)SettingsData.MESSAGES_DELIMETER);
            string message;
            if (delimiterIndex < 0)
            {
                overheadString = string.Empty;
                message = response;
            }
            else
            {
                message = response.Substring(0, delimiterIndex);
                if (delimiterIndex < response.Length && delimiterIndex > 0)
                    overheadString = response.Substring(delimiterIndex + 1);
                else
                    overheadString = string.Empty;
            }
            return message;
        }

        private void InvokeValidCallback(string message)
        {
            if (_responseCallbacks == null)
                throw new ArgumentException(SettingsData.NO_VALID_CALLBACK);
            bool atLeastOneCalled = false;
            foreach (var element in _responseCallbacks)
            {
                var deserialisedObject = _xmlSerializator.DeserializeFromXML(message, element.Item1);
                if (deserialisedObject != null)
                {
                    element.Item2.Invoke(deserialisedObject);
                    atLeastOneCalled = true;
                }
            }
            if (!atLeastOneCalled)
                throw new ArgumentException(SettingsData.NO_VALID_CALLBACK);
        }
    }
}