﻿using System;
using System.Collections.Generic;

namespace Communication
{
    using MessagesCallbacksCollection = Dictionary<Type, IncomingMessageCallback>;
    using ObjectType = ICollection<Tuple<object, Type>>;
    public delegate ObjectType IncomingMessageCallback(object message);
    /// <summary>
    /// Manager for listening for incoming connections and invoking valid callbacks.
    /// Callback returns collection of pairs in the form of [object,type]. Object is the response
    /// object and the type is the type of this object. Each item in the collection 
    ///  will be serialized to XML message and send as a response.
    /// It is guaranteed that the object in the argument will be the type of Type.
    /// </summary>
    public interface IConnectionManager
    {
        MessagesCallbacksCollection IncomingMessagesCallbacks { get; set; }
        int Port { get; set; }

        void StartListening();
        void StopListening();
    }
}