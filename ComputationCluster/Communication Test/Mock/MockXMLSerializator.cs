﻿using Communication;
using System;

namespace ConnectionManagerShould.Mock
{
    class MockMessageObjectXMLSerializator : IXMLSerializator
    {
        public object DeserializeFromXML(string xmlString, Type objectType)
        {
            var xmlObject = new MockMessageObject { Id = 44, IdSpecified = true };
            return xmlObject;
        }

        public string SerializeToXML(object objectToSerialize, Type objectType)
        {
            return "<?xml version=\"1.0\" encoding=\"utf-8\"?>\r\n<MockMessageObject xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns=\"http://www.mini.pw.edu.pl/ucc/\">\r\n  <Type>TaskManager</Type>\r\n  <ParallelThreads>0</ParallelThreads>\r\n  <Id>44</Id>\r\n</MockMessageObject>";
        }
    }
}