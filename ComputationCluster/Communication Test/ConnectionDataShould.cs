﻿using Communication;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace ConnectionManagerShould
{
    [TestClass]
    public class ConnectionDataShould
    {
        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void ConstructorThrowFormatExceptionOnBadIpAddress()
        {
            var badAddress = "192.129.129.129.129";
            int port = 1222;
            var connectionData = new ConnectionData(badAddress, port);
        }
    }
}
