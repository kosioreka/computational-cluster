﻿using Communication;
using ConnectionManagerShould.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ConnectionManagerShould
{
    [TestClass]
    public class XMLSerializatorShould
    {
        [TestMethod]
        public void DeserializeFromXML()
        {
            var xmlSerializator = new XMLSerializator();
            var mockSerializator = new MockMessageObjectXMLSerializator();
            var mocKMessage = mockSerializator.SerializeToXML(null, null);
            var expectedResult = mockSerializator.DeserializeFromXML(null, null) as MockMessageObject;
            var result = xmlSerializator.DeserializeFromXML(mocKMessage, typeof(MockMessageObject)) as MockMessageObject;
            Assert.AreEqual(expectedResult.Id, result.Id);
        }

        [TestMethod]
        public void SerializeToXML()
        {
            var mockSerializator = new MockMessageObjectXMLSerializator();
            var messageObject = mockSerializator.DeserializeFromXML(null, null) as MockMessageObject;
            var xmlSerializator = new XMLSerializator();
            var result = xmlSerializator.SerializeToXML(messageObject, typeof(MockMessageObject));
            var expectedResult = mockSerializator.SerializeToXML(null, null);
            Assert.AreEqual(expectedResult, result);
        }
    }
}