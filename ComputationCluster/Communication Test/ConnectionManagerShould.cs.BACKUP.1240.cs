﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Communication;
using ConnectionManagerShould.Mock;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ConnectionManagerShould
{
    [TestClass]
    public class ConnectionManagerShould
    {
        [TestMethod]
        public async Task InvokeCallbackOnIncomingCommunicationAndSendResponse()
        {
            var mockSerializator = new MockMessageObjectXMLSerializator();
            var port = 13001;
            var connectionData = new ConnectionData("127.0.0.1", port);
            bool wasInMessageCallback = false;
            var messageCallback = new IncomingMessageCallback((message) =>
            {
                var messageObject = message as MockMessageObject;
                var expected = mockSerializator.DeserializeFromXML(null, null) as MockMessageObject;
                Assert.AreEqual(expected.Id, messageObject.Id);
                var responses = new List<Tuple<object, Type>>();
                responses.Add(new Tuple<object, Type>(expected, typeof(MockMessageObject)));
                wasInMessageCallback = true;
                return responses;
            });
            var callbacksCollection = new Dictionary<Type, IncomingMessageCallback>();
            callbacksCollection.Add(typeof(MockMessageObject), messageCallback);
            var connectionManager = new ConnectionManager(mockSerializator);
            connectionManager.IncomingMessagesCallbacks = callbacksCollection;
            connectionManager.Port = port;
            connectionManager.StartListening();
            var callbacks = new List<Tuple<Type, ResponseCallback>>();
            bool wasInResponseCallback = false;
            var responseCallback = new ResponseCallback((response) =>
            {
                var responseObject = response as MockMessageObject;
                var expected = mockSerializator.DeserializeFromXML(null, null) as MockMessageObject;
                Assert.AreEqual(expected.Id, responseObject.Id);
                wasInResponseCallback = true;
            });
            callbacks.Add(new Tuple<Type, ResponseCallback>(typeof(MockMessageObject), responseCallback));
<<<<<<< HEAD
            var messageSenderManager = new MessageSenderManager(connectionData, mockSerializator);
=======
            var svh = new ServersHierarchyManager(connectionData);
            var messageSenderManager = new MessageSenderManager(mockSerializator, svh);
>>>>>>> 013a931b110b0ec917b76a41f0a5ea1541fc2fa6
            var toSend = mockSerializator.DeserializeFromXML(null, null) as MockMessageObject;
            await messageSenderManager.SendMessage(toSend, callbacks);
            Assert.IsTrue(wasInMessageCallback);
            Assert.IsTrue(wasInResponseCallback);
            connectionManager.StopListening();
        }

        [TestMethod]
        public async Task InvokeValidCallbackOnMessageAndSendResponse()
        {
            var serializator = new XMLSerializator();
            var port = 13001;
            var connectionData = new ConnectionData("127.0.0.1", port);
            bool wasInMessage1Callback = false;
            bool wasInMessage2Callback = false;
            var message1Callback = new IncomingMessageCallback((message) =>
            {
                var messageObject = message as MockMessageObject;
                var expected = new MockMessageObject();
                var responses = new List<Tuple<object, Type>>();
                responses.Add(new Tuple<object, Type>(expected, typeof(MockMessageObject)));
                wasInMessage1Callback = true;
                return responses;
            });
            var message2Callback = new IncomingMessageCallback((message) =>
            {
                var messageObject = message as MockMessageObject2;
                Assert.IsNotNull(messageObject);
                uint expected = 999;
                Assert.AreEqual(expected, messageObject.ComputationalNodes);
                var response = new MockMessageObject();
                var responses = new List<Tuple<object, Type>>();
                responses.Add(new Tuple<object, Type>(response, typeof(MockMessageObject)));
                wasInMessage2Callback = true;
                return responses;
            });
            var callbacksCollection = new Dictionary<Type, IncomingMessageCallback>();
            callbacksCollection.Add(typeof(MockMessageObject), message1Callback);
            callbacksCollection.Add(typeof(MockMessageObject2), message2Callback);
            var connectionManager = new ConnectionManager(serializator);
            connectionManager.IncomingMessagesCallbacks = callbacksCollection;
            connectionManager.Port = port;
            connectionManager.StartListening();
            var callbacks = new List<Tuple<Type, ResponseCallback>>();
            bool wasInSenderCallback = false;
            var responseCallback = new ResponseCallback((response) =>
            {
                var responseObject = response as MockMessageObject;
                Assert.IsNotNull(responseObject);
                wasInSenderCallback = true;
            });
            callbacks.Add(new Tuple<Type, ResponseCallback>(typeof(MockMessageObject), responseCallback));
<<<<<<< HEAD
            var messageSenderManager = new MessageSenderManager(connectionData, serializator);
=======
            var svh = new ServersHierarchyManager(connectionData);
            var messageSenderManager = new MessageSenderManager(serializator, svh);
>>>>>>> 013a931b110b0ec917b76a41f0a5ea1541fc2fa6
            var toSend = new MockMessageObject2();
            toSend.ComputationalNodes = 999;
            await messageSenderManager.SendMessage(toSend, callbacks);
            Assert.IsFalse(wasInMessage1Callback);
            Assert.IsTrue(wasInMessage2Callback);
            Assert.IsTrue(wasInSenderCallback);
            connectionManager.StopListening();
        }

        [TestMethod]
        public async Task HandleManyConnections()
        {
            var serializator = new XMLSerializator();
            var port = 13001;
            var connectionData = new ConnectionData("127.0.0.1", port);
            var message1Callback = new IncomingMessageCallback((message) =>
            {
                var messageObject = message as MockMessageObject;
                var responses = new List<Tuple<object, Type>>();
                responses.Add(new Tuple<object, Type>(messageObject, typeof(MockMessageObject)));
                return responses;
            });
            var callbacksCollection = new Dictionary<Type, IncomingMessageCallback>();
            callbacksCollection.Add(typeof(MockMessageObject), message1Callback);
            var connectionManager = new ConnectionManager(serializator);
            connectionManager.IncomingMessagesCallbacks = callbacksCollection;
            connectionManager.Port = port;
            connectionManager.StartListening();

            bool wasInSenderCallback1 = false;
            var callbacks1 = new List<Tuple<Type, ResponseCallback>>();
            var responseCallback1 = new ResponseCallback((response) =>
            {
                var responseObject = response as MockMessageObject;
                Assert.AreEqual(RegisterType.CommunicationServer, responseObject.Type);
                wasInSenderCallback1 = true;
            });
            callbacks1.Add(new Tuple<Type, ResponseCallback>(typeof(MockMessageObject), responseCallback1));
            var toSend1 = new MockMessageObject();
            toSend1.Type = RegisterType.CommunicationServer;
<<<<<<< HEAD
            var messageSenderManager1 = new MessageSenderManager(connectionData, serializator);
=======
            var svh = new ServersHierarchyManager(connectionData);
            var messageSenderManager1 = new MessageSenderManager(serializator, svh);
>>>>>>> 013a931b110b0ec917b76a41f0a5ea1541fc2fa6
            var task1 = messageSenderManager1.SendMessage(toSend1, callbacks1);

            bool wasInSenderCallback2 = false;
            var callbacks2 = new List<Tuple<Type, ResponseCallback>>();
            var responseCallback2 = new ResponseCallback((response) =>
            {
                var responseObject = response as MockMessageObject;
                Assert.AreEqual(RegisterType.ComputationalNode, responseObject.Type);
                wasInSenderCallback2 = true;
            });
            callbacks2.Add(new Tuple<Type, ResponseCallback>(typeof(MockMessageObject), responseCallback2));
            var toSend2 = new MockMessageObject();
            toSend2.Type = RegisterType.ComputationalNode;
<<<<<<< HEAD
            var messageSenderManager2 = new MessageSenderManager(connectionData, serializator);
=======
            var svh2 = new ServersHierarchyManager(connectionData);
            var messageSenderManager2 = new MessageSenderManager(serializator, svh2);
>>>>>>> 013a931b110b0ec917b76a41f0a5ea1541fc2fa6
            var task2 = messageSenderManager2.SendMessage(toSend2, callbacks2);

            await task1;
            await task2;
            Assert.IsTrue(wasInSenderCallback1);
            Assert.IsTrue(wasInSenderCallback2);
            connectionManager.StopListening();
        }
    }
}