﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ConnectionManagerShould.Mock;
using Communication;
using System.Net.Sockets;
using System.Threading.Tasks;
using System.Text;
using System.Threading;
using System.Diagnostics;
using System.Collections.Generic;

namespace ConnectionManagerShould
{
    [TestClass]
    public class MessageSenderManagerShould
    {
        [TestMethod]
        public async Task InvokeCallbackOnEachResponseReceived()
        {
            var mockSerializator = new MockMessageObjectXMLSerializator();
            var connectionData = new ConnectionData("127.0.0.1", 13000);
            var svh = new ServersHierarchyManager(connectionData);
            var messageSender = new MessageSenderManager(mockSerializator, svh);
            var cancellationTokenSource = new CancellationTokenSource();
            var listener = new TcpListener(connectionData.IPAddress, connectionData.Port);
            var callbacks = new List<Tuple<Type, ResponseCallback>>();
            int numberOfCallbacks = 0;
            var responseCallback = new ResponseCallback((response) =>
            {
                var responseObject = response as MockMessageObject;
                var expected = mockSerializator.DeserializeFromXML(null, null) as MockMessageObject;
                Assert.AreEqual(expected.Id, responseObject.Id);
                numberOfCallbacks++;
            });
            callbacks.Add(new Tuple<Type, ResponseCallback>(typeof(MockMessageObject), responseCallback));
            listener.Start();
            AcceptClientAsync(listener, cancellationTokenSource.Token);
            await messageSender.SendMessage<object>(null, callbacks);
            cancellationTokenSource.Cancel();
            Assert.AreEqual(2, numberOfCallbacks);
        }

        private async Task AcceptClientAsync(TcpListener listener, CancellationToken cancellationToken)
        {
            using (var client = await listener.AcceptTcpClientAsync().ConfigureAwait(false))
            {
                using (var networkStream = client.GetStream())
                {
                    var bytes = new byte[256];
                    var incomingMessage = new StringBuilder();
                    var timeoutTask = Task.Delay(TimeSpan.FromSeconds(5));
                    var amountReadTask = networkStream.ReadAsync(bytes, 0, bytes.Length, cancellationToken);
                    var completedTask = await Task.WhenAny(timeoutTask, amountReadTask)
                                                  .ConfigureAwait(false);
                    while (completedTask != timeoutTask)
                    {
                        var read = await amountReadTask;
                        incomingMessage.Append(Encoding.UTF8.GetString(bytes, 0, read));
                        if (read != 0)
                            timeoutTask = Task.Delay(TimeSpan.FromSeconds(1));
                        amountReadTask = networkStream.ReadAsync(bytes, 0, bytes.Length, cancellationToken);
                        completedTask = await Task.WhenAny(timeoutTask, amountReadTask).ConfigureAwait(false);
                    }
                    var finalMessage = incomingMessage.ToString();
                    var byteResponse = Encoding.UTF8.GetBytes(finalMessage);
                    await networkStream.WriteAsync(byteResponse, 0, byteResponse.Length);
                    var delimeter_char = new char[] { (char)23 };
                    var delimeter_bytes = Encoding.UTF8.GetBytes(delimeter_char);
                    await networkStream.WriteAsync(delimeter_bytes, 0, delimeter_bytes.Length);
                    await networkStream.WriteAsync(byteResponse, 0, byteResponse.Length);
                }
            }
        }
    }
}