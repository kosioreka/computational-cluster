﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ComputationalNode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication;

namespace ComputationalNode.Tests
{
    [TestClass()]
    public class CNodeProgramTests
    {
        [TestMethod()]
        public void CNodeProgramConstructorTest()
        {
            CNodeProgram cn = new CNodeProgram();
            Assert.IsNotNull(cn);
        }

        [TestMethod()]
        public void CNPropertyID_StoresCorrectly()
        {
            CNodeProgram cn = new CNodeProgram();
            cn.Id = 999;
            Assert.AreEqual((ulong)999, cn.Id);
        }

        [TestMethod()]
        public void CNPropertyTimeout_StoresCorrectly()
        {
            CNodeProgram cn = new CNodeProgram();
            cn.Timeout = 10;
            Assert.AreEqual((uint)10, cn.Timeout);
        }

        [TestMethod()]
        public void CNSendRegisterMessgTest()
        {
            CNodeProgram cn = new CNodeProgram();
            bool flag = Task.Factory.StartNew(cn.SendRegisterMessg).Wait(TimeSpan.FromSeconds(1));
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void CNSendRegisterMessgTest2()
        {
            CNodeProgram cn = new CNodeProgram();
            bool flag = Task.Factory.StartNew(cn.SendRegisterMessg).Wait(100);
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void CNSendRegisterMessgTest3()
        {
            try
            {
                CNodeProgram cn = new CNodeProgram();
                bool flag = cn.SendRegisterMessg().Wait(10);
                if (flag == false)
                    Assert.IsFalse(flag);
                else
                    Assert.IsTrue(flag);
            }
            catch (AggregateException ae)
            {
                Assert.AreEqual("Sth bad happened during Register method execution.", ae.Message);
            }
            catch (Exception e)
            {
                Assert.Fail(string.Format("Unexpected exception of type {0} caught: {1}", e.GetType(), e.Message)
                );
            }
        }

        [TestMethod()]
        public void CNSendStatusMessgTest()
        {
            CNodeProgram cn = new CNodeProgram();
            bool flag = Task.Factory.StartNew(cn.SendStatusMessg).Wait(TimeSpan.FromSeconds(1));
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void CNSendStatusMessgTest1()
        {
            CNodeProgram cn = new CNodeProgram();
            Task t = Task.Factory.StartNew(cn.SendStatusMessg);
            bool flag = t.Wait(5);
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void CNSendSolutionsMessgTest()
        {
            CNodeProgram cn = new CNodeProgram();
            Task t = Task.Factory.StartNew(() => cn.SendSolutionsMessg(null));
            bool flag = t.Wait(100);
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
            
        }
    }
}