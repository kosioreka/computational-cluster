﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Communication;
using System.Linq;
using System.Threading;
using System.Text;
using System.IO;
using System.Diagnostics;

namespace ComputationalClient
{
    public class CClientProgram
    {
        static ulong processId;

        static List<Problem> problemsToSolve = new List<Problem>();
        public static bool isFinalSolutionObtained = true;

        static IServersHierarchyManager _serversHierarchyManager =
            new ServersHierarchyManager(new ConnectionData("127.0.0.1", 13000));
        static Stopwatch _stopWatch;


        static void Main(string[] args)
        {
            CClientProgram.delay(2);

            Console.WriteLine("Give the number of clients:");
            int NR = readInput();

            var filePath = "h:\\Windows7\\Desktop\\DVRP\\DVRP Problems\\" + "io2_" + NR + "_plain_a_D.vrp";
            var dataText = File.ReadAllText(filePath);

            var problem = new Problem()
            {
                ProblemType = "Trudny problem",
                //problem to fill
                Data = Encoding.ASCII.GetBytes(dataText),
                Status = ProblemStatus.ToCompute
            };
            _stopWatch = Stopwatch.StartNew();
            SendSolveRequestMessg(problem).Wait();

            Console.ReadKey();
        }

        private static int readInput()
        {
            int NR;
            while (true)
            {
                var input = Console.ReadLine();
                int.TryParse(input, out NR);

                if(NR<4 || NR > 16)
                {
                    Console.WriteLine("Bad usage! Give one number: [4, 16]");
                }
                else
                {
                    break;
                }
            }
            return NR;
        }

        private static async Task SendSolveRequestMessg(Problem problem)
        {
            var connectionData = new ConnectionData("127.0.0.1", 13000);
            var msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
            var d = new List<Tuple<Type, ResponseCallback>>();

            var solveRequest = new SolveRequest()
            {
                Data = problem.Data,
                ProblemType = problem.ProblemType
            };
            if (problem.ID != 0)
            {
                solveRequest.Id = problem.ID;
                solveRequest.IdSpecified = true;
            }
            else
            {
                solveRequest.IdSpecified = false;
            }
            if (problem.SolvingTimeout != 0)
            {
                solveRequest.SolvingTimeout = problem.SolvingTimeout;
                solveRequest.SolvingTimeoutSpecified = true;
            }
            else
            {
                solveRequest.SolvingTimeoutSpecified = false;
            }

            d.Add(new Tuple<Type, ResponseCallback>(typeof(SolveRequestResponse), (response) =>
            {
                var solve = response as SolveRequestResponse;
                processId = solve.Id;
                problem.ID = solve.Id;
                problem.Status = ProblemStatus.Computing;
                if (!problemsToSolve.Contains(problem))
                {
                    problemsToSolve.Add(problem);
                }
                Console.WriteLine("[CC]: Problem received ID: {0}", solve.Id);
            }));

            await msm.SendMessage(solveRequest, d);
            Console.WriteLine("[CC]: Message SolveRequest sent");
            delay(5);
            SendSolutionRequestMessg(problem.ID).Wait();

        }


        private static async Task SendSolutionRequestMessg(ulong id)
        {
            while (isFinalSolutionObtained)
            {
                var connectionData = new ConnectionData("127.0.0.1", 13000);
                var msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
                var message = new SolutionRequest() { Id = id };
                var d = new List<Tuple<Type, ResponseCallback>>();
                d.Add(new Tuple<Type, ResponseCallback>(typeof(Solutions), (response) =>
                {
                    var solutionsMessage = response as Solutions;
                    var solved = solutionsMessage.Solutions1;
                    if (id != solutionsMessage.Id)
                    {
                        //e
                    }
                    if (solved.Length == 0 || solved == null || solved.Any(s => s.Type != SolutionsSolutionType.Final))
                    {
                        Console.Write("[CC] CS is still computing.");
                    }
                    else
                    {

                        isFinalSolutionObtained = false;
                        Console.WriteLine("[CC] Problem is computed!");
                        Console.WriteLine(String.Format("Time of calculations: {0}", _stopWatch.Elapsed));

                        var computedProblem = problemsToSolve.Find(p => p.ID == solutionsMessage.Id);
                        if (computedProblem == null)
                        {
                            //e
                        }
                        else
                        {
                            computedProblem.PartialSolutions = new List<PartialSolution>();
                            foreach (var s in solved)
                            {
                                computedProblem.PartialSolutions.Add(new PartialSolution()
                                {
                                    Data = s.Data,
                                    TaskId = s.TaskId,
                                    Status = ProblemStatus.Done,
                                    TimeoutOccurd = false
                                });
                            }
                            computedProblem.Status = ProblemStatus.Done;
                            var solution = Encoding.UTF8.GetString(solutionsMessage.Solutions1[0].Data);
                            
                            Console.WriteLine(solution);

                        }
                    }


                }));
                d.Add(new Tuple<Type, ResponseCallback>(typeof(Error), (response) =>
                {
                    var errorReceived = response as Error;
                    Console.WriteLine("[CC]: ErrorMsg is received: {0}", errorReceived.ErrorType.ToString());
                    if (errorReceived.ErrorType == ErrorErrorType.UnknownSender || errorReceived.ErrorType == ErrorErrorType.InvalidOperation)
                    {
                        Task.Factory.StartNew(() => SendSolveRequestMessg(problemsToSolve.Find(p => p.ID == id)));
                    }

                }));
                await msm.SendMessage(message, d);
                Console.WriteLine("[CC]: Message SolutionRequest in sent.");
                delay(5);
            }
        }

        private static void delay(int arg)
        {
            int sec = arg * 1000;
            if (sec == 0) sec++;

            System.Threading.Thread.Sleep(sec);
        }
    }
}
