﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text;
using Algorithms;
using System.Collections.Generic;
using System.IO;

namespace AlgorithmsTests
{
    [TestClass]
    public class DVRPTests
    {
        [TestMethod]
        public void AlgorithmWorksProperly()
        {
            int nodesCount = 3;
            string filePath = "io2_4_plain_a_D.vrp";
            var dataText = File.ReadAllText(filePath);
            var dvrpSolver = new Solver(Encoding.ASCII.GetBytes(dataText));
            var dividedProblems = dvrpSolver.DivideProblem(nodesCount);
            var partialSolutions = new List<byte[]>();
            for (int i = 0; i < nodesCount; i++)
            {
                var partialResult = dvrpSolver.Solve(dividedProblems[i], TimeSpan.FromHours(4)); //timeout na razie nie ma znaczenia
                partialSolutions.Add(partialResult);
            }
            var finalSolution = dvrpSolver.MergeSolution(partialSolutions.ToArray());

            var solution = Encoding.UTF8.GetString(finalSolution);
            Assert.AreEqual("Cost: 304,396993287007\n3, 2\n0, 1\n", solution);
        }
    }
}
