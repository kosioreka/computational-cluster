﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Communication;
using System.Threading;
using System.Linq;
using Algorithms;
using System.Text;

namespace TaskManager
{
    public class TaskManager
    {
        private ulong id;
        private uint timeout;
        private List<StatusThread> _threads;
        public string[] SolvableProblems { get; set; }

        public List<StatusThread> Threads
        {
            get { return _threads; }
            set { _threads = value; }
        }

        public ulong Id
        {
            get { return id; }
            set { id = value; }
        }

        public uint Timeout
        {
            get { return timeout; }
            set { timeout = value; }
        }

        private IServersHierarchyManager _serversHierarchyManager = new ServersHierarchyManager(new ConnectionData("192.168.143.84", 13000));

        static void Main(string[] args)
        {
            TaskManager TM = new TaskManager();

            TM.SendRegisterMsgAndGetRegisterResponseMsgAsync().Wait();

            var task1 = TM.SendStatusMsgAndGetMsgAsync();
            //var task2 = TM.Work();

            Task.WaitAll(task1);
        }

        public TaskManager()
        {
            this.Threads = new List<StatusThread>()
            {
                new StatusThread() {State = StatusThreadState.Idle, ProblemType="Trudny problem"},
                new StatusThread() {State = StatusThreadState.Idle, ProblemType="Trudny problem"},
                new StatusThread() {State = StatusThreadState.Idle, ProblemType="Trudny problem"}
            };
            this.SolvableProblems = this
                .Threads
                .Select(t => t.ProblemType)
                .ToArray();
        }

        public async Task SendRegisterMsgAndGetRegisterResponseMsgAsync()
        {
            MessageSenderManager msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
            var message = new Register()
            {
                Type = RegisterType.TaskManager,
                IdSpecified = false,
                ParallelThreads = (byte)Threads.Count,
                SolvableProblems = SolvableProblems
            };
            var d = new List<Tuple<Type, ResponseCallback>>();
            d.Add(new Tuple<Type, ResponseCallback>(typeof(RegisterResponse), (response) =>
            {
                var responseMessage = response as RegisterResponse;
                Console.WriteLine("[TM]: Otrzymałem odpowiedź na wiadomość Register typu: {0}, moje id: {1}, timeout przysłany: {2}",
                    response.ToString(), responseMessage.Id, responseMessage.Timeout);
                Timeout = responseMessage.Timeout;
                Id = responseMessage.Id;
            }));
            await msm.SendMessage(message, d);
        }

        public async Task SendStatusMsgAndGetMsgAsync()
        {
            while (true)
            {
                Console.WriteLine("[TM {0}]: Wysyłam status do mastera", Id);
                MessageSenderManager msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
                var message = new Status() { Id = this.Id, Threads = Threads.ToArray() };
                var d = new List<Tuple<Type, ResponseCallback>>();

                //obsluga DivideRequestMsg
                d.Add(new Tuple<Type, ResponseCallback>(typeof(DivideProblem), (response) =>
                {
                    var divideProblemMessage = response as DivideProblem;
                    Console.WriteLine("[TM {0}]: Otrzymałem prośbę podziału na podproblemy", Id);
                    Console.WriteLine("[TM {0}]: Zaczynam podział problemu {1} na Partial Problems.", Id, divideProblemMessage.Id);
                    //podział Problemu na PartialProblems
                    //TODO: threads:
                    //StatusThread thread;
                    //if ((thread = Threads.FirstOrDefault(t=> t.State == StatusThreadState.Idle))!= null)
                    //{
                    //    thread.State = StatusThreadState.Busy;
                    //    thread.TaskId = divideProblemMessage.Id;
                    //}
                    //argumentem wywolywanej ponizej funckji ma byc zamockowany PP, wstawic cokolwiek, argumentem powinna byc tablica ale chcecie zmiencie, bo nie wiem co na serwie zrobicie
                    Task.Factory.StartNew(()=>DivideProblemAlg(divideProblemMessage));
                   // DivideProblemAlg(divideProblemMessage);
                }));


                //oblsuga SolutionsMsg
                d.Add(new Tuple<Type, ResponseCallback>(typeof(Solutions), (response) =>
                {
                    var solutionsMessage = response as Solutions;
                    Console.WriteLine("[TM {0}]: Otrzymałem odpowiedź na wiadomość Status typu: {0}",
                        Id, response.ToString());
                    Console.WriteLine("[TM {0}]: Zaczynam procedurę wyboru najlepszego Solution problemy: {0}.",
                        Id, solutionsMessage.Id);
                    solutionsMessage.Solutions1[0].Type = SolutionsSolutionType.Final;
                    //wybór BESTSOLUTION
                    //argumentem wywolywanej ponizej funckji ma byc zamockowany Solution, wstawic cokolwiek
                    Task.Factory.StartNew(() => SendBestSolutionMsgAsync(solutionsMessage));
                }));

                //obsluga ErrorMsg
                d.Add(new Tuple<Type, ResponseCallback>(typeof(Error), (response) =>
                {
                    Console.WriteLine("[TM {0}]: Otrzymałem ErrorMsg!!!", Id);
                    var errorReceived = response as Error;
                    if (errorReceived.ErrorType == ErrorErrorType.UnknownSender)
                    {
                        Task.Factory.StartNew(() => SendRegisterMsgAndGetRegisterResponseMsgAsync());
                    }

                }));

                //obsluga BackupServersMsg
                await msm.SendMessage(message, d);             
                await Task.Delay((int)Timeout);
            }
        }

        public async Task SendPartialProblemMsgAsync(SolvePartialProblems message)
        {
            MessageSenderManager msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
            var d = new List<Tuple<Type, ResponseCallback>>();
            //obsluga backup servers

            //obsluga ErrorMsg
            d.Add(new Tuple<Type, ResponseCallback>(typeof(Error), (response) =>
            {
                Console.WriteLine("[TM]: Otrzymałem ErrorMsg!!!");
                var errorReceived = response as Error;
                if (errorReceived.ErrorType == ErrorErrorType.UnknownSender)
                {
                    Task.Factory.StartNew(() => SendRegisterMsgAndGetRegisterResponseMsgAsync());
                }
                if (errorReceived.ErrorType == ErrorErrorType.InvalidOperation)
                {
                    //Nothing happens.
                }

            }));
            Console.WriteLine("[TM]: Wysyłam Partial Problem.");
            await msm.SendMessage(message, d);
        }

        private async Task SendBestSolutionMsgAsync(Solutions solutions)
        {
            MessageSenderManager msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
            var solver = new Solver(solutions.CommonData);
            var list = new List<byte[]>();
            foreach(var b in solutions.Solutions1)
            {
                list.Add(b.Data);
            }
            var bestSolution = solver.MergeSolution(list.ToArray());
            var message = new Solutions()
            {

                Solutions1 = new SolutionsSolution[]
                    {
                        new SolutionsSolution { Data = bestSolution,
                            TaskId = 0, TaskIdSpecified = false,
                            Type = SolutionsSolutionType.Final }
                    }, 
                Id = solutions.Id,
                ProblemType = solutions.ProblemType
            };

            var d = new List<Tuple<Type, ResponseCallback>>();
            d.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers), (response) =>
            {
                Console.WriteLine("[TM]: Otrzymałem informacje o Backup Serverach {0}", response.ToString());
            }));
            Console.WriteLine("[TM]: Wysyłam Best Solution {0}.", message.Id);
            await msm.SendMessage(message, d);
        }

        private void DivideProblemAlg(DivideProblem problem)
        {
            var solver = new Solver(problem.Data);
            var dividedProblems = solver.DivideProblem(3);
            var partialProblems = new SolvePartialProblems()
            {
                PartialProblems = new SolvePartialProblemsPartialProblem[problem.Data.Length],
                Id = problem.Id, //id wiadomości to id problemu!
                ProblemType = problem.ProblemType,
                CommonData = problem.Data
            };


            int i = 0;
            foreach (var p in dividedProblems)
            {
                partialProblems.PartialProblems[i] = new SolvePartialProblemsPartialProblem()
                {
                    Data = dividedProblems[i],
                    TaskId = (ulong)i+1, //TM ustawia id podproblemów!
                    NodeID = problem.NodeID
                };
                i++;
            }
            Task.Factory.StartNew(() => SendPartialProblemMsgAsync(partialProblems));

        }


        public async Task Work()
        {
            while (true)
            {
                Thread.Sleep((int)Timeout);
                Console.WriteLine("*");
            }
        }

        public void Delay(int arg)
        {
            int sec = arg;
            if (sec == 0) sec++;

            System.Threading.Thread.Sleep(sec);
        }
    }
}