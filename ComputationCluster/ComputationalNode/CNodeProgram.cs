﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Communication;
using Algorithms;

namespace ComputationalNode
{
    public class CNodeProgram
    {
        private uint _timeout;
        private ulong _id;
        private StatusThreadState _statusThreadState = StatusThreadState.Idle;

        public ulong Id { get { return _id; } set { _id = value; } }
        public uint Timeout { get { return _timeout; } set { _timeout = value; } }
        private IServersHierarchyManager _serversHierarchyManager 
            = new ServersHierarchyManager(new ConnectionData("192.168.143.84", 13000));

        static void Main(string[] args)
        {
            var program = new CNodeProgram();
            program.Delay(2);

            program.SendRegisterMessg().Wait();
            var s = program.SendStatusMessg();

            Task.WaitAll(s);
        }

        public CNodeProgram()
        {

        }

        public async Task SendRegisterMessg()
        {
            MessageSenderManager msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
            var message = new Register() { Type = RegisterType.ComputationalNode, IdSpecified = false };
            var d = new List<Tuple<Type, ResponseCallback>>();
            d.Add(new Tuple<Type, ResponseCallback>(typeof(RegisterResponse), (response) =>
            {

                var registerResponse = response as RegisterResponse;
                Console.WriteLine("[CN]: Otrzymałem odpowiedź, {0}", response.ToString());
                try
                {
                    _timeout = checked((uint)registerResponse.Timeout);  
                }
                catch (System.OverflowException e)
                {
                    System.Console.WriteLine(e.ToString());
                }
                Id = registerResponse.Id;
                Console.WriteLine("[CN]: Mój przydzielony nr id to: {0}. Będę o sobie przypominał co {1}", _id, _timeout);
            }));
            Console.WriteLine("Wiadomość REGISTER wysłana");
            await msm.SendMessage(message, d);
        }

        public async Task SendStatusMessg()
        {
            while (true)
            {
                var statusThread = new StatusThread();
                statusThread.State = _statusThreadState;
                var statusThreads = new StatusThread[1];
                statusThreads[0] = statusThread;
                var msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
                var message = new Status() { Id = this.Id, Threads = statusThreads};
                var d = new List<Tuple<Type, ResponseCallback>>();
                d.Add(new Tuple<Type, ResponseCallback>(typeof(SolvePartialProblems), (response) =>
                {
                    var problems = response as SolvePartialProblems;
                    Console.WriteLine("[CN]: Otrzymałem problemy do rozwiązania: {0}", response.ToString());
                    _statusThreadState = StatusThreadState.Busy;
                    Task.Factory.StartNew(() => ComputeSolvePartialProblems(problems));
                    
                }));
                d.Add(new Tuple<Type, ResponseCallback>(typeof(Error), (response) =>
                {
                    var errorReceived = response as Error;
                    Console.WriteLine("[CN]: Otrzymałem ErrorMsg: {0}", errorReceived.ErrorType.ToString());
                    if (errorReceived.ErrorType == ErrorErrorType.UnknownSender)
                    {
                        Task.Factory.StartNew(() => SendRegisterMessg());
                    }

                }));
                Console.WriteLine("[CN]: Wiadomość STATUS wysłana");
                await msm.SendMessage(message, d);
                await Task.Delay((int)Timeout);
            }
        }

        public async Task SendSolutionsMessg(Solutions message)
        {
            var msm = new MessageSenderManager(new XMLSerializator(), _serversHierarchyManager);
            var d = new List<Tuple<Type, ResponseCallback>>();
            d.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers), (response) =>
            {
                Console.WriteLine("[CN]: Otrzymałem odpowiedź na wiadomość solution typu: {0}", response.ToString());
            }));
            d.Add(new Tuple<Type, ResponseCallback>(typeof(Error), (response) =>
            {
                var errorReceived = response as Error;
                Console.WriteLine("[CN]: Otrzymałem ErrorMsg: {0}", errorReceived.ErrorType.ToString());
                if (errorReceived.ErrorType == ErrorErrorType.UnknownSender)
                {
                    Task.Factory.StartNew(() => SendRegisterMessg());
                }
                else
                {
                    //nothing?
                }

            }));
            Console.WriteLine("[CN]: Wiadomość SOLUTION wysłana");
            await msm.SendMessage(message, d);
        }

        private void ComputeSolvePartialProblems(SolvePartialProblems problem)
        {
            var solution = new Solutions
            {
                Solutions1 = new SolutionsSolution[problem.PartialProblems.Length],
                Id = problem.Id,//id wiadomości to id problemu!
                ProblemType = problem.ProblemType,
                CommonData = problem.CommonData
            };

            int i = 0;
            var solver = new Solver(problem.CommonData);


            foreach (SolvePartialProblemsPartialProblem p in problem.PartialProblems)
            {
                var solvedProblems = solver.Solve(p.Data, TimeSpan.FromHours(358));
                Console.WriteLine("[CN]: Problem nr: {0}, z problemem: {1}", p.TaskId, Encoding.UTF8.GetString(p.Data));
                solution.Solutions1[i++] = new SolutionsSolution()
                {
                    TaskId = (ulong)p.TaskId,//id podsolucji dane przez CN od 1!
                    TaskIdSpecified = true,
                    Data = solvedProblems,
                    Type = SolutionsSolutionType.Partial
                };
            }
            _statusThreadState = StatusThreadState.Idle;
            Task.Factory.StartNew(() => SendSolutionsMessg(solution));

        }

        private void Delay(int arg)
        {
            int sec = arg * 1000;
            if (sec == 0) sec++;

            System.Threading.Thread.Sleep(sec);
        }
    }
}
