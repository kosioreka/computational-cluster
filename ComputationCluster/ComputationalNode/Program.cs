﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication;

namespace ComputationalNode
{
    class Program
    {
        static void Main(string[] args)
        {
            MessageSenderManager msm = new MessageSenderManager(new XMLSerializator()) {};
            var message = new Register() { Type = RegisterType.ComputationalNode, IdSpecified = false };
            var d = new Dictionary<Type, ResponseCallback>();
            d.Add(typeof(Register), (response) => Console.WriteLine("Otrzymałem odpowiedź, {0}", response.ToString()));
            msm.SendMessage(new ConnectionData("127.0.0.1", 13000), message, d);
            Console.WriteLine("Wiadomość wysłana");
            Console.ReadKey();
        }
    }
}
