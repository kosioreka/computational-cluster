﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication;
using System.Threading;

namespace CommunicationServer
{
    public class CommunicationServer
    {
        #region private fields
        private List<Component> _registeredComponents;
        private List<Problem> _problemsToSolve;

        private ulong _lastComponentID = 1;
        private ulong _lastProblemID = 0;
        private uint _statusTimeout;
        private uint _myTimeout = 3000;
        
        private ConnectionData _originalMaster, _masterServerData, _serverData;
        private List<Tuple<object, Type>> _messagesToSend = new List<Tuple<object, Type>>();
        private List<BackupCommunicationServer> _backupServers =
                    new List<BackupCommunicationServer>();
        private uint _backupServerTimeout;
        private ulong _id;
        private Task _statusTask;
        private bool _isBackupServer = false;
        private ulong _backupChildServerId;
        private IServersHierarchyManager _serversHierarchyManager;
        #endregion

        #region properties
        /// <summary>
        /// List of all Components being active and registered to this CS
        /// </summary>
        public List<Component> RegisteredComponents
        {
            get
            {
                return _registeredComponents;
            }
            set
            {
                _registeredComponents = value;
            }
        }

        /// <summary>
        /// List of all problems sent to this CS by CC 
        /// </summary>
        public List<Problem> ProblemsToSolve
        {
            get
            {
                return _problemsToSolve;
            }
            set
            {
                _problemsToSolve = value;
            }
        }
        /// <summary>
        /// Last ID given to component
        /// </summary>
        public ulong LastComponentID
        {
            get
            {
                return _lastComponentID;
            }
            set
            {
                _lastComponentID = value;
            }
        }

        /// <summary>
        /// Last ID given to problem
        /// </summary>
        public ulong LastProblemID
        {
            get
            {
                return _lastProblemID;
            }
            set
            {
                _lastProblemID = value;
            }
        }
        #endregion

        public void Work(ConnectionData serverData, uint timeout, ConnectionData masterServer = null)
        {
            _statusTimeout = timeout;
            RegisteredComponents = new List<Component>();
            ProblemsToSolve = new List<Problem>();
            var xmlSerializator = new XMLSerializator();
            var connectionManager = new ConnectionManager(xmlSerializator) { Port = serverData.Port };
            var callbacks = new Dictionary<Type, IncomingMessageCallback>();
            callbacks.Add(typeof(Register), RegisterCallback);
            callbacks.Add(typeof(Status), StatusCallback);
            callbacks.Add(typeof(Solutions), SolutionsCallback);
            callbacks.Add(typeof(SolveRequest), SolveRequestCallback);
            callbacks.Add(typeof(SolutionRequest), SolutionRequestCallback);
            callbacks.Add(typeof(SolvePartialProblems), SolvePartialProblemsCallback);
            connectionManager.IncomingMessagesCallbacks = callbacks;
            connectionManager.StartListening();

            _serverData = serverData;
            if (masterServer != null)
            {
                _isBackupServer = true;
                _originalMaster = masterServer;
                ChangeMaster(masterServer);
                _statusTask = BackupServerStartup(xmlSerializator);
            }
            else
                Console.WriteLine("[CS]: Master serwer online");

            Console.ReadKey();
        }
        
        #region callbacks definitions

        /// <summary>
        /// Callback to Register message sent by all components
        /// </summary>
        /// <param name="message">Message object of type Register</param>
        /// <returns>Collection of responses performed after receiveing such message</returns>
        private ICollection<Tuple<object, Type>> RegisterCallback(object message)
        {
            var register = message as Register;
            var responses = new List<Tuple<object, Type>>();
            var component = GetComponent(register);
            RegisteredComponents.Add(component);
            
            if (register.Type == RegisterType.CommunicationServer &&
                register.IdSpecified &&
                register.Id == _id)
            {
                return responses;
            }
            else if (register.Type == RegisterType.CommunicationServer)
            {
                RegisterBackupServer(register, component);
            }
            else
            {
                DeregisterOnTimeout(component);
            }

            //Send further down the hierarchy register message.
            register.IdSpecified = true;
            register.Id = component.ID;
            _messagesToSend.Add(new Tuple<object, Type>(register, typeof(Register)));

            Console.WriteLine("[CS {1}]: Serwer dostał registra, komponent dostaje ID={0}",
                component.ID, _id);

            var registerResponse = new RegisterResponse() { Id = component.ID, Timeout = _statusTimeout};           
            var backupServersMessage = GenerateBackupServersMessage();

            responses.Add(new Tuple<object, Type>(registerResponse, typeof(RegisterResponse)));
            responses.Add(backupServersMessage);
            return responses;
        }
       
        /// <summary>
        /// Callback to Status message sent by all components
        /// </summary>
        /// <param name="message">Message object of type Status</param>
        /// <returns>Collection of responses performed after receiveing such message</returns>
        private ICollection<Tuple<object, Type>> StatusCallback(object message)
        {
            var status = message as Status;
            var responses = new List<Tuple<object, Type>>();
            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);

            if (status.Id == _backupChildServerId)
            {
                //Current backup server
                //Console.WriteLine("[CS {0}]: Backupowy serwer przyslal status", _id);
                ResetComponentTimeout(status);
                responses.AddRange(_messagesToSend);
                _messagesToSend.Clear();
                return responses;
            }
            else if (RegisteredComponents.Exists((s) => s.ID == status.Id &&
                         s.Type == Component.ComponentType.CommunicationServer))
            {
                //Not current backup server, our backupserver died
                Console.WriteLine("[CS {0}]: Backupowy serwer umarł, przyjmuje, ze moim nowym jest {1}",
                    _id, status.Id);
                _backupChildServerId = status.Id;
                responses.AddRange(_messagesToSend);
                _messagesToSend.Clear();
                return responses;
            }
            if (!RegisteredComponents.Exists(c => c.ID == status.Id))
            {
                //Komponent niezarejestrowany. Odrzucam połączenie.
                var error = new Error() { ErrorType = ErrorErrorType.UnknownSender };
                return responses;
            }
            if (_isBackupServer)
            {
                Console.WriteLine("[CS {0}]: Stałem się nowym masterem", _id);
            }

            _isBackupServer = false;
            var activeComponent = RegisteredComponents.Find(c => c.ID == status.Id);
            ResetComponentTimeout(status);
            Console.WriteLine("[{0}] Przyszedł sygnał, nie deregistruje, timeout odnowiony", activeComponent.ID);
            Console.WriteLine("[CS {1}]: Przesyłam backup serwery do {0}", activeComponent.ID, _id);

            switch (activeComponent.Type)
            {
                case Component.ComponentType.TaskManager:
                    var problemToDivide = ProblemsToSolve
                        .Where(p => p.Status == ProblemStatus.ToDivide)
                        .FirstOrDefault();

                    var problemWithSolutions = ProblemsToSolve
                        .Where(p => p.Status == ProblemStatus.GetFinalSolution)
                        .FirstOrDefault();

                    //PROBLEM TO DIVIDE
                    //if both not null then get with smaller id.
                    if (((problemToDivide != null && problemWithSolutions != null) &&
                        (problemToDivide.ID < problemWithSolutions.ID)) ||
                        (problemWithSolutions == null && problemToDivide != null))
                    {
                        Console.WriteLine("[{0}] Wysyłam do TMa prośbę o podział na podproblemy", problemToDivide.ID);
                        var divideProblemMessage = new DivideProblem()
                        {
                            ProblemType = problemToDivide.ProblemType,
                            Data = problemToDivide.Data,
                            Id = problemToDivide.ID,
                            NodeID = activeComponent.ID
                        };
                        problemToDivide.TMID = activeComponent.ID;
                        problemToDivide.Status = ProblemStatus.Dividing;
                        responses.Add(new Tuple<object, Type>(divideProblemMessage, typeof(DivideProblem)));
                        _messagesToSend.Add(new Tuple<object, Type>(divideProblemMessage, typeof(DivideProblem)));
                    }

                    //SOLUTION TO CHOOSE
                    if (((problemToDivide != null && problemWithSolutions != null) && (problemToDivide.ID > problemWithSolutions.ID)) ||
                            (problemWithSolutions != null && problemToDivide == null))
                    {
                        var partialSolutions = problemWithSolutions.PartialSolutions;
                        if (partialSolutions != null)
                        {
                            var solutionMessage = new Solutions()
                            {
                                Id = problemWithSolutions.ID,
                                CommonData = problemWithSolutions.Data,
                                ProblemType = problemWithSolutions.ProblemType,
                            };

                            solutionMessage.Solutions1 = new SolutionsSolution[partialSolutions.Count];

                            int i = 0;
                            foreach (var sol in partialSolutions)
                            {
                                solutionMessage.Solutions1[i++] = new SolutionsSolution()
                                {
                                    TaskId = sol.TaskId,
                                    TaskIdSpecified = true,
                                    Data = sol.Data,
                                    Type = SolutionsSolutionType.Partial
                                };
                            }
                            problemWithSolutions.TMID = activeComponent.ID;
                            problemWithSolutions.Status = ProblemStatus.GettingFinalSolution;
                            responses.Add(new Tuple<object, Type>(solutionMessage, typeof(Solutions)));
                        }
                    }
                    break;
                //PROBLEM TO COMPUTE
                case Component.ComponentType.ComputationalNode:
                    if (status.Threads.FirstOrDefault().State == StatusThreadState.Busy)
                        break;
                    var problemToCompute = ProblemsToSolve.FirstOrDefault(p => p.Status == ProblemStatus.ToCompute);//tzn istenieje chociaż jeden podproblem toCompute
                    if (problemToCompute != null)
                    {
                        var partialProblemToCompute = problemToCompute
                            .PartialProblems
                            .FirstOrDefault(pp => pp.Status == ProblemStatus.ToCompute);

                        if (partialProblemToCompute != null)
                        {
                            var solvePartialProblemsMessage = new SolvePartialProblems()
                            {
                                Id = problemToCompute.ID,
                                ProblemType = problemToCompute.ProblemType,
                                CommonData = problemToCompute.Data,
                                //TO DO: w przyszlosci bedzie wiele pp to rozwiazania a nie tylko jeden
                                PartialProblems = new SolvePartialProblemsPartialProblem[1]
                                {
                                        new SolvePartialProblemsPartialProblem()
                                        {
                                            Data = partialProblemToCompute.Data,
                                            NodeID = activeComponent.ID,
                                            TaskId = partialProblemToCompute.TaskId
                                        }
                                }
                            };
                            partialProblemToCompute.NodeId = activeComponent.ID;
                            partialProblemToCompute.Status = ProblemStatus.Computing;
                            //problemToCompute.UpdateProblemStatus();//??? TODO:if last ToCompute podproblem
                            if (problemToCompute.PartialProblems.All(p=> p.Status == ProblemStatus.Computing || p.Status == ProblemStatus.Done))
                            {
                                //problemToCompute.Status = ProblemStatus.Computing;
                            }
                            responses.Add(new Tuple<object, Type>(solvePartialProblemsMessage, typeof(SolvePartialProblems)));
                        }
                    }
                    break;
            }

            return responses;
        }
        
        /// <summary>
        /// Callback to SolutionRequest messaage sent by CC to get actual status of computations
        /// </summary>
        /// <param name="message">Message object of type SolutionRequest</param>
        /// <returns>Collection of responses performed after receiveing such message</returns>
        private ICollection<Tuple<object, Type>> SolutionRequestCallback(object message)
        {
            var responses = new List<Tuple<object, Type>>();
            var solutionRequest = message as SolutionRequest;
            var problemToSolve = ProblemsToSolve.FirstOrDefault(p => p.ID == solutionRequest.Id);

            Console.WriteLine("[CS]: Klient się przypomina.");
            if (problemToSolve != null)
            {
                var solution = new Solutions()
                {
                    Id = problemToSolve.ID,
                    CommonData = problemToSolve.Data,
                    ProblemType = problemToSolve.ProblemType,
                    Solutions1 = new SolutionsSolution[1],
                };

                int i = 0;

                if (problemToSolve.Status == ProblemStatus.ReadyToSend)
                {
                    PartialSolution sol = problemToSolve.BestSolution.FirstOrDefault();
                    solution.Solutions1[0] = new SolutionsSolution { Data = sol.Data, TaskId = sol.TaskId, Type = SolutionsSolutionType.Final };
                    problemToSolve.Status = ProblemStatus.Sent;
                }
                else if(problemToSolve.Status == ProblemStatus.Sent)
                {
                    //CC is still requesting but CC already got it
                    PartialSolution sol = problemToSolve.BestSolution.FirstOrDefault();
                    solution.Solutions1[0] = new SolutionsSolution { Data = sol.Data, TaskId = sol.TaskId, Type = SolutionsSolutionType.Final };
                }
                else
                {
                    solution.Solutions1[0] = new SolutionsSolution { Type = SolutionsSolutionType.Ongoing };
                }
                responses.Add(new Tuple<object, Type>(solution, typeof(Solutions)));
            }
            else
            {
                //e
            }
            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            return responses;
        }
        
        /// <summary>
        /// Callback to SolvePartialProblem message sent by TM after dividing problem into subproblems
        /// </summary>
        /// <param name="message">Message object of type SolvePartialProblem</param>
        /// <returns>Collection of responses performed after receiveing such message</returns>
        private ICollection<Tuple<object, Type>> SolvePartialProblemsCallback(object message)
        {
            var solvePartialProblemsMessage = message as SolvePartialProblems;
            var responses = new List<Tuple<object, Type>>();
            Console.WriteLine("Serwer otrzymał podzielone problemy");

            var problem = ProblemsToSolve.FirstOrDefault(p => p.ID == solvePartialProblemsMessage.Id);//problem znaleziony
            //ADDING PARTIAL PROBLEMS from TM to the main problem
            if (problem != null)
            {
                foreach (var p in solvePartialProblemsMessage.PartialProblems)
                {
                    problem.PartialProblems.Add(new PartialProblem()
                    {
                        TaskId = p.TaskId,//TM ustawił id podproblemów
                        Data = p.Data,
                        Status = ProblemStatus.ToCompute,
                    });
                }
                problem.Status = ProblemStatus.ToCompute;
            }
            else
            {
                //e
            }

            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            _messagesToSend.Add(new Tuple<object, Type>(message, typeof(SolvePartialProblems)));

            return responses;
        }
        
        /// <summary>
        /// Callback to Solutions message sent to CS from CN (after computing partial problem) and TM (after creating final solution).
        /// </summary>
        /// <param name="message">Message object of type Solution</param>
        /// <returns>Collection of responses performed after receiveing such message</returns>
        private ICollection<Tuple<object, Type>> SolutionsCallback(object message)
        {
            var solutions = message as Solutions;
            var responses = new List<Tuple<object, Type>>();
            var problem = ProblemsToSolve.FirstOrDefault(p => p.ID == solutions.Id);

            _messagesToSend.Add(new Tuple<object, Type>(message, typeof(Solutions)));

            if (problem != null)
            {
                //GETTING computed SOLUTION from CN
                if (problem.Status == ProblemStatus.Computing || problem.Status == ProblemStatus.ToCompute)
                {
                        foreach (var solution in solutions.Solutions1)
                        {
                            problem.PartialSolutions.Add(new PartialSolution
                            {
                                Data = solution.Data,
                                TaskId = solution.TaskId,
                                Status = ProblemStatus.GetFinalSolution
                            });
                            problem.PartialProblems.First(pp => pp.TaskId == solution.TaskId).Status = ProblemStatus.Done;
                        }
                        //STATUS check
                        if(problem.PartialProblems.All(p =>p.Status == ProblemStatus.Done))
                        {
                            problem.Status = ProblemStatus.GetFinalSolution;
                        }
                        Console.WriteLine("Serwer otrzymał rozwiązania do wysłanych problemów");
                    }
                //Getting final solution from TM
                else if (problem.Status == ProblemStatus.GettingFinalSolution)
                {
                    foreach (var solution in solutions.Solutions1)
                    {
                        problem.BestSolution.Add(new PartialSolution()
                        {
                            Data = solution.Data,
                            Status = ProblemStatus.Done
                        });
                    }
                    problem.Status = ProblemStatus.ReadyToSend;
                }
            }
            else
            {
                //e
            }    
         
            var backupServers = GenerateBackupServersMessage();
            responses.Add(backupServers);
            return responses;
        }
        
        /// <summary>
        /// Solve request sent by CC
        /// SolveRequestResponse as reponse to CC
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private ICollection<Tuple<object, Type>> SolveRequestCallback(object message)
        {
            //ADDING NEW PROBLEM from CC
            var request = message as SolveRequest;
            var responses = new List<Tuple<object, Type>>();
            ulong id = request.IdSpecified ? request.Id : ++LastProblemID;

            var newProblem = new Problem()
            {
                ID = id,
                Data = request.Data,
                ProblemType = request.ProblemType,
                Status = ProblemStatus.ToDivide,
                SolvingTimeout = request.SolvingTimeoutSpecified ? 0 : request.SolvingTimeout
            };
            ProblemsToSolve.Add(newProblem);

            Console.WriteLine("[Id Problemu: {0}] Server otrzymał problem od klienta. Typ problemu: {1}",
                newProblem.ID, newProblem.ProblemType);

            _messagesToSend.Add(new Tuple<object, Type>(message, typeof(SolveRequest)));
            var response = new SolveRequestResponse() { Id = newProblem.ID };
            var backupServersMessage = GenerateBackupServersMessage();

            responses.Add(new Tuple<object, Type>(response, typeof(SolveRequestResponse)));
            responses.Add(backupServersMessage);
            return responses;
        }

        #endregion
        
        #region backup handling
        private void ChangeMaster(ConnectionData newMasterData)
        {
            _masterServerData = newMasterData;
            _serversHierarchyManager = new ServersHierarchyManagerForServer(_masterServerData,
                MasterServerNotResponding);
            Console.WriteLine("[CSB {0}]: Mam nowego mastera na adresie {1}:{2}",
                _id, _masterServerData.IPAddress.ToString(), _masterServerData.Port);
        }

        private void MasterServerNotResponding()
        {
            var oldMasterIndex = _backupServers.FindIndex((t) =>
                t.address == _masterServerData.IPAddress.ToString() &&
                t.port == _masterServerData.Port);
            if (oldMasterIndex == 0)
            {
                ChangeMaster(_originalMaster);
            }
            else if (oldMasterIndex < 0)
            {
                Console.WriteLine("[CS {0}]: Stałem się nowym masterem", _id);
                _isBackupServer = false;
                return;
            }
            else
            {
                var newMaster = new ConnectionData(_backupServers[oldMasterIndex - 1].address,
                    _backupServers[oldMasterIndex - 1].port);
                ChangeMaster(newMaster);
            }
        }


        private async Task BackupServerStartup(IXMLSerializator xmlSerializator)
        {
            Console.WriteLine("[CBS]: Backup online");
            var messageSender = new MessageSenderManager(xmlSerializator, _serversHierarchyManager, _serverData);
            var register = new Register();
            register.Type = RegisterType.CommunicationServer;
            var responsesCallbacks = new List<Tuple<Type, ResponseCallback>>();
            responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(RegisterResponse), (response) =>
            {
                var responseMessage = response as RegisterResponse;
                Console.WriteLine("[CSB]: Otrzymałem odpowiedź na wiadomość Register typu:" +
                    "{0}, moje id: {1}, timeout przysłany: {2}",
                    response.ToString(), responseMessage.Id, responseMessage.Timeout);
                _backupServerTimeout = responseMessage.Timeout;
                _id = responseMessage.Id;
            }));
            responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers), (response) =>
            {
                var backupServers = response as BackupServers;
                Console.WriteLine("[CSB {0}]: BackupServers od mastera", _id);
                _backupServers = backupServers.BackupCommunicationServers.ToList();
                var myIndex = _backupServers.FindIndex(
                    (s) =>
                    {
                        return s.address == _serverData.IPAddress.ToString() &&
                        s.port == _serverData.Port;
                    });
                if (myIndex == 0)
                {
                    //First master child
                    return;
                }
                var newMaster = new ConnectionData(_backupServers[myIndex - 1].address,
                    _backupServers[myIndex - 1].port);
                ChangeMaster(newMaster);
            }));
            await messageSender.SendMessage(register, responsesCallbacks);
            _statusTask = SendStatusMessage(xmlSerializator);
        }

        private void RegisterBackupServer(Register register, Component component)
        {
            if (_backupChildServerId == 0)
                _backupChildServerId = component.ID;
            if (!_isBackupServer)
            {
                var backupServerData = new BackupCommunicationServer();
                backupServerData.address = register.ConnectionParameters.Address.ToString();
                backupServerData.port = (ushort)register.ConnectionParameters.Port;
                int index;
                if ((index = _backupServers.FindIndex((s) => s.address == backupServerData.address &&
                        s.port == backupServerData.port)) != -1)
                {
                    Console.WriteLine("[CS {0}]: Nowe polaczenie z danego adresu, przyjmuje, ze stary umarl",
                        _id);
                    _backupServers.RemoveAt(index);
                }
                _backupServers.Add(backupServerData);
            }
        }
        
        private Tuple<object, Type> GenerateBackupServersMessage()
        {
            var backupServers = new BackupServers();
            backupServers.BackupCommunicationServers = _backupServers.ToArray();
            return new Tuple<object, Type>(backupServers, typeof(BackupServers));
        }
        
        private void AddDeregisterMessageToBackup(ulong id)
        {
            var deregister = new Register();
            deregister.DeregisterSpecified = true;
            deregister.Deregister = true;
            deregister.IdSpecified = true;
            deregister.Id = id;
            _messagesToSend.Add(new Tuple<object, Type>(deregister, typeof(Register)));
        }

        #endregion

        #region handling timeout
        private void ResetComponentTimeout(Status status)
        {
            var activeComponent = RegisteredComponents.Find(c => c.ID == status.Id);
            if (activeComponent.TokenSource != null)
                activeComponent.TokenSource.Cancel();
            activeComponent.Threads = status.Threads;
            DeregisterOnTimeout(activeComponent);
        }

        public void DeregisterOnTimeout(Component component)
        {
            var id = component.ID;
            component.TokenSource = new CancellationTokenSource();
            component.Token = component.TokenSource.Token;
            var token = component.Token;
            component.TimeoutDeregister = new Task(() =>
            {
                Thread.Sleep((int)_myTimeout + 4000);
                if (token.IsCancellationRequested)
                    return;
                if (!_isBackupServer)
                {
                    switch (component.Type)
                    {
                        case Component.ComponentType.TaskManager:
                            var problemsDuringDividing = ProblemsToSolve
                                .Where(p => p.TMID == id && p.Status == ProblemStatus.Dividing);
                            foreach (var p in problemsDuringDividing)
                            {
                                p.TMID = 0;
                                p.Status = ProblemStatus.ToDivide;
                            }

                            var problemsDuringGettingFinalSolution = ProblemsToSolve
                                .Where(p => p.TMID == id && p.Status == ProblemStatus.GettingFinalSolution);
                            foreach (var p in problemsDuringGettingFinalSolution)
                            {
                                p.TMID = 0;
                                p.Status = ProblemStatus.GetFinalSolution;
                            }
                            break;
                        case Component.ComponentType.ComputationalNode:
                            var problemsDuringComputing = ProblemsToSolve
                                .Where(p => p.Status == ProblemStatus.Computing);
                            foreach (var p in problemsDuringComputing)
                            {
                                var partialProblemsDuringComputing = p
                                    .PartialProblems
                                    .Where(pp => pp.NodeId == component.ID);

                                foreach (var pp in partialProblemsDuringComputing)
                                {
                                    pp.NodeId = 0;
                                    p.Status = ProblemStatus.ToCompute;
                                }
                            }
                            break;
                    }
                    Console.WriteLine("[Id Komponentu: {0}] Przekroczono timeout w registrze. Wyrejestrowuje komponent.",
                        id);
                    RegisteredComponents.Remove(RegisteredComponents.Find(c => c.ID == id));
                    AddDeregisterMessageToBackup(id);
                }
            }, component.Token);
            component.TimeoutDeregister.Start();
        }
        #endregion

        private async Task SendStatusMessage(IXMLSerializator xmlSerializator)
        {
            while (true)
            {
                await Task.Delay((int)_myTimeout);
                var messageSender = new MessageSenderManager(xmlSerializator,
                    _serversHierarchyManager, _serverData);
                var status = new Status();
                status.Id = _id;
                var responsesCallbacks = new List<Tuple<Type, ResponseCallback>>();
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(Register), (response) =>
                {
                    var registerMessage = response as Register;
                    if (registerMessage.DeregisterSpecified && registerMessage.Deregister)
                    {
                        //Console.WriteLine("[CSB {1}]: Deregistruje {0} od mastera",
                        //registerMessage.Id, _id);
                        var component = RegisteredComponents.Find(c => c.ID == registerMessage.Id);
                        RegisteredComponents.Remove(component);
                        if (component.TokenSource != null)
                            component.TokenSource.Cancel();
                        AddDeregisterMessageToBackup(registerMessage.Id);
                        return;
                    }
                    RegisterCallback(response);
                    Console.WriteLine("[CSB {1}]: Rejestruje {0} od mastera", registerMessage.Id, _id);
                }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(SolveRequest),
                    (response) =>
                    {
                        var solveRequest = response as SolveRequest;
                        Console.WriteLine("[CSB {0}]: SolveRequest od mastera", _id);
                        SolveRequestCallback(response);
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(Solutions),
                    (response) =>
                    {
                        var solutions = response as Solutions;
                        Console.WriteLine("[CSB {0}]: Solutions od mastera", _id);
                        SolutionsCallback(response);
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers),
                    (response) =>
                    {
                        var backupServers = response as BackupServers;
                        Console.WriteLine("[CSB {0}]: BackupServers od mastera", _id);
                        _backupServers = backupServers.BackupCommunicationServers.ToList();
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(DivideProblem),
                    (response) =>
                    {
                        var divideProblem = response as DivideProblem;
                        var problemToDivide = ProblemsToSolve
                            .Where(p => p.ID == divideProblem.Id)
                            .FirstOrDefault();
                        problemToDivide.TMID = divideProblem.NodeID;
                        problemToDivide.Status = ProblemStatus.Dividing;
                        Console.WriteLine("[CSB {0}]: DivideProblem od mastera", _id);
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(SolvePartialProblems),
                    (response) =>
                    {
                        var partialProblems = response as SolvePartialProblems;
                        var problem = ProblemsToSolve
                            .Where(p => p.ID == partialProblems.PartialProblems[0].TaskId)
                            .FirstOrDefault();
                        if (problem != null)
                        {
                            int i = 0;
                            foreach (var p in partialProblems.PartialProblems)
                            {
                                problem.PartialProblems.Add(new PartialProblem()
                                {
                                    TaskId = partialProblems.PartialProblems[i].TaskId,
                                    Data = p.Data,
                                    Status = ProblemStatus.ToCompute,
                                });
                                i++;
                            }
                            problem.Status = ProblemStatus.ToCompute;
                        }
                        else
                        {
                            var partialProblemToCompute = problem.PartialProblems
                                .FirstOrDefault(pp => pp.TaskId == partialProblems.PartialProblems[0].TaskId);
                            partialProblemToCompute.NodeId = partialProblems.PartialProblems[0].NodeID;
                            partialProblemToCompute.Status = ProblemStatus.Computing;
                            problem.UpdateProblemStatus();
                        }
                        Console.WriteLine("[CSB {0}]: PartialProblems od mastera", _id);
                    }));
                Console.WriteLine("[CSB {0}]: Wysylam status do mastera", _id);
                await messageSender.SendMessage(status, responsesCallbacks);
                if (!_isBackupServer)
                    return;
            }
        }

        private Component GetComponent(Register register)
        {
            Component component;
            if (register.IdSpecified)
            {
                component = new Component()
                {
                    ID = register.Id,
                    Type = Component.mapToCType(register.Type)
                };
            }
            else
            {
                component = new Component()
                {
                    ID = LastComponentID++,
                    Type = Component.mapToCType(register.Type)
                };
            }
            return component;
        }
    }
}