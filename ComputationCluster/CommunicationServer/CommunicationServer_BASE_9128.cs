﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication;
using System.Threading;

namespace CommunicationServer
{
    public class CommunicationServer
    {
        #region private fields
        //private List<ComputationalClientComponent> _computationalClientComponents;
        //private List<ComputationalNodeComponent> _computationalNodeComponents;
        //private List<CommunicationServerComponent> _communicationServerComponents;
        //private List<TaskManagerComponent> _taskManagerComponents;
        private List<Component> _registeredComponents;
        private ulong _lastComponentID = 1;
        private ulong _lastProblemID = 0;
        private uint _timeout = 3000;
        private List<Problem> _problemsToSolve;

        private ConnectionData _originalMaster, _masterServerData, _serverData;
        private List<Tuple<object, Type>> _messagesToSend = new List<Tuple<object, Type>>();
        private List<BackupCommunicationServer> _backupServers =
                    new List<BackupCommunicationServer>();
        private uint _backupServerTimeout;
        private ulong _id;
        private Task _statusTask;
        private bool _isBackupServer = false;
        private ulong _backupChildServerId;
        private IServersHierarchyManager _serversHierarchyManager;
        #endregion

        #region properties
        public List<Component> RegisteredComponents
        {
            get
            {
                return _registeredComponents;
            }
            set
            {
                _registeredComponents = value;
            }
        }
        //public List<ComputationalClientComponent> ComputationalClientComponents { get; set; }
        //public List<ComputationalNodeComponent> ComputationalNodeComponents { get; set; }
        //public List<CommunicationServerComponent> CommunicationServerComponents { get; set; }
        //public List<TaskManagerComponent> TaskManagerComponents { get; set; }
        public ulong LastComponentID
        {
            get
            {
                return _lastComponentID;
            }
            set
            {
                _lastComponentID = value;
            }
        }

        public ulong LastProblemID
        {
            get
            {
                return _lastProblemID;
            }
            set
            {
                _lastProblemID = value;
            }
        }
        /// <summary>
        /// problems for TM (deleted when TM gets the problem)
        /// </summary>
        public List<Problem> ProblemsToSolve
        {
            get
            {
                return _problemsToSolve;
            }
            set
            {
                _problemsToSolve = value;
            }
        }
        /// <summary>
        /// Problems got form CC (deleted when CC receives the solution)
        /// </summary>
        #endregion


        public void Work(ConnectionData serverData, ConnectionData masterServer = null)
        {
            RegisteredComponents = new List<Component>();
            ProblemsToSolve = new List<Problem>();
            var xmlSerializator = new XMLSerializator();
            var connectionManager = new ConnectionManager(xmlSerializator) { Port = serverData.Port };
            var callbacks = new Dictionary<Type, IncomingMessageCallback>();
            callbacks.Add(typeof(Register), RegisterCallback);
            callbacks.Add(typeof(Status), StatusCallback);
            callbacks.Add(typeof(Solutions), SolutionsCallback);
            callbacks.Add(typeof(SolveRequest), SolveRequestCallback);
            callbacks.Add(typeof(SolutionRequest), SolutionRequestCallback);
            callbacks.Add(typeof(SolvePartialProblems), SolvePartialProblemsCallback);
            connectionManager.IncomingMessagesCallbacks = callbacks;
            connectionManager.StartListening();

            _serverData = serverData;
            if (masterServer != null)
            {
                _isBackupServer = true;
                _originalMaster = masterServer;
                ChangeMaster(masterServer);
                _statusTask = BackupServerStartup(xmlSerializator);
            }
            else
                Console.WriteLine("[CS]: Master serwer online");

            Console.ReadKey();
        }

        private ICollection<Tuple<object, Type>> SolvePartialProblemsCallback(object message)
        {
            var solvePartialProblemsMessage = message as SolvePartialProblems;
            var responses = new List<Tuple<object, Type>>();
            Console.WriteLine("Serwer otrzymał podzielone problemy");

            var problem = ProblemsToSolve.FirstOrDefault(p => p.ID == solvePartialProblemsMessage.Id);

            if (problem != null)
            {
                foreach (var p in solvePartialProblemsMessage.PartialProblems)
                {
                    problem.PartialProblems.Add(new PartialProblem()
                    {
                        TaskId = p.TaskId,
                        Data = p.Data,
                        Status = ProblemStatus.ToCompute
                    });
                }
                problem.Status = ProblemStatus.ToCompute;
            }

            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            _messagesToSend.Add(new Tuple<object, Type>(message, typeof(SolvePartialProblems)));

            return responses;
        }

        private void ChangeMaster(ConnectionData newMasterData)
        { 
            _masterServerData = newMasterData;
            _serversHierarchyManager = new ServersHierarchyManagerForServer(_masterServerData,
                MasterServerNotResponding);
            Console.WriteLine("[CSB {0}]: Mam nowego mastera na adresie {1}:{2}",
                _id, _masterServerData.IPAddress.ToString(), _masterServerData.Port);
        }

        private void MasterServerNotResponding()
        {
            var oldMasterIndex = _backupServers.FindIndex((t) =>
                t.address == _masterServerData.IPAddress.ToString() &&
                t.port == _masterServerData.Port);
            if(oldMasterIndex == 0)
            {
                ChangeMaster(_originalMaster);
                //I am new master
            }
            else if(oldMasterIndex < 0)
            {
                Console.WriteLine("[CS {0}]: Stałem się nowym masterem", _id);
                _isBackupServer = false;
                return;
            }
            else
            {
                var newMaster = new ConnectionData(_backupServers[oldMasterIndex - 1].address,
                    _backupServers[oldMasterIndex - 1].port);
                ChangeMaster(newMaster);
            }
        }

        private void ConnectToNewMasterServer()
        {

        }

        private async Task BackupServerStartup(IXMLSerializator xmlSerializator)
        {
            //TODO: Backups failures and propagation
            Console.WriteLine("[CBS]: Backup online");
            var messageSender = new MessageSenderManager(xmlSerializator, _serversHierarchyManager, _serverData);
            var register = new Register();
            register.Type = RegisterType.CommunicationServer;
            var responsesCallbacks = new List<Tuple<Type, ResponseCallback>>();
            responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(RegisterResponse), (response) =>
            {
                var responseMessage = response as RegisterResponse;
                Console.WriteLine("[CSB]: Otrzymałem odpowiedź na wiadomość Register typu:" + 
                    "{0}, moje id: {1}, timeout przysłany: {2}",
                    response.ToString(), responseMessage.Id, responseMessage.Timeout);
                _backupServerTimeout = responseMessage.Timeout;
                _id = responseMessage.Id;
            }));
            responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers), (response) =>
            {
                var backupServers = response as BackupServers;
                Console.WriteLine("[CSB {0}]: BackupServers od mastera", _id);
                _backupServers = backupServers.BackupCommunicationServers.ToList();
                var myIndex = _backupServers.FindIndex(
                    (s) =>
                    {
                        return s.address == _serverData.IPAddress.ToString() &&
                        s.port == _serverData.Port;
                    });
                if (myIndex == 0)
                {
                    //First master child
                    return;
                }
                var newMaster = new ConnectionData(_backupServers[myIndex - 1].address,
                    _backupServers[myIndex - 1].port);
                ChangeMaster(newMaster);
            }));
            await messageSender.SendMessage(register, responsesCallbacks);
            _statusTask = SendStatusMessage(xmlSerializator);
        }

        private async Task SendStatusMessage(IXMLSerializator xmlSerializator)
        {
            while (true)
            {
                await Task.Delay((int)_timeout);
                var messageSender = new MessageSenderManager(xmlSerializator,
                    _serversHierarchyManager, _serverData);
                var status = new Status();
                status.Id = _id;
                var responsesCallbacks = new List<Tuple<Type, ResponseCallback>>();
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(Register), (response) =>
                {
                    var registerMessage = response as Register;
                    if (registerMessage.DeregisterSpecified && registerMessage.Deregister)
                    {
                        Console.WriteLine("[CSB {1}]: Deregistruje {0} od mastera",
                            registerMessage.Id, _id);
                        var component = RegisteredComponents.Find(c => c.ID == registerMessage.Id);
                        if (component == null)
                            return;
                        RegisteredComponents.Remove(component);
                        if(component.TokenSource != null)
                            component.TokenSource.Cancel();
                        AddDeregisterMessageToBackup(registerMessage.Id);
                        return;
                    }
                    RegisterCallback(response);
                    Console.WriteLine("[CSB {1}]: Rejestruje {0} od mastera", registerMessage.Id, _id);
                }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(SolveRequest), 
                    (response) =>
                    {
                        var solvePartialProblems = response as SolveRequest;
                        Console.WriteLine("[CSB {0}]: SolveRequest od mastera", _id);
                        SolveRequestCallback(response);
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(Solutions), 
                    (response) =>
                    {
                        var solutions = response as Solutions;
                        Console.WriteLine("[CSB {0}]: Solutions od mastera", _id);
                        SolutionsCallback(response);
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(BackupServers),
                    (response) =>
                    {
                        var backupServers = response as BackupServers;
                        Console.WriteLine("[CSB {0}]: BackupServers od mastera", _id);
                        _backupServers = backupServers.BackupCommunicationServers.ToList();
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(DivideProblem), 
                    (response) =>
                    {
                        var divideProblem = response as DivideProblem;
                        Console.WriteLine("[CSB {0}]: DivideProblem od mastera", _id);
                    }));
                responsesCallbacks.Add(new Tuple<Type, ResponseCallback>(typeof(SolvePartialProblems), 
                    (response) =>
                    {
                        var partialProblems = response as SolvePartialProblems;
                        Console.WriteLine("[CSB {0}]: PartialProblems od mastera", _id);
                    }));
                //TODO: DivideRequest, SolvePartialProblem 
                Console.WriteLine("[CSB {0}]: Wysylam status do mastera", _id);
                await messageSender.SendMessage(status, responsesCallbacks);
                if (!_isBackupServer)
                    return;
            }
        }

        private Component GetComponent(Register register)
        {
            Component component;
            if (register.IdSpecified)
            {
                component = new Component()
                {
                    ID = register.Id,
                    Type = Component.mapToCType(register.Type)
                };
            }
            else
            {
                component = new Component()
                {
                    ID = LastComponentID++,
                    Type = Component.mapToCType(register.Type)
                };
            }
            return component;
        }

        /// <summary>
        /// </summary>
        /// <param name="register"></param>
        /// <param name="component"></param>
        /// <returns>True if server is responsible for this element timeout, false otherwise</returns>
        private bool RegisterBackupServer(Register register, Component component)
        {
            bool setDeregisterTimeout = true;
            if (_backupChildServerId == 0)
                _backupChildServerId = component.ID;
            else
                setDeregisterTimeout = false;
            if (!_isBackupServer)
            {
                var backupServerData = new BackupCommunicationServer();
                backupServerData.address = register.ConnectionParameters.Address.ToString();
                backupServerData.port = (ushort)register.ConnectionParameters.Port;
                int index;
                if ((index = _backupServers.FindIndex((s) => s.address == backupServerData.address &&
                        s.port == backupServerData.port)) != -1)
                {
                    Console.WriteLine("[CS {0}]: Nowe polaczenie z danego adresu, przyjmuje, ze stary umarl",
                        _id);
                    _backupServers.RemoveAt(index);
                }
                _backupServers.Add(backupServerData);
            }
            return setDeregisterTimeout;
        }

        private ICollection<Tuple<object, Type>> RegisterCallback(object message)
        {
            var register = message as Register;
            var component = GetComponent(register);
            RegisteredComponents.Add(component);
            var responses = new List<Tuple<object, Type>>();
            bool setDeregisterTimeout = true;
            if (register.Type == RegisterType.CommunicationServer && register.IdSpecified && 
                register.Id == _id)
                return responses;
            if (register.Type == RegisterType.CommunicationServer)
            {
                setDeregisterTimeout = RegisterBackupServer(register, component);
            }
            //Send further down the hierarchy register message.
            register.IdSpecified = true;
            register.Id = component.ID;
            _messagesToSend.Add(new Tuple<object, Type>(register, typeof(Register)));
            if (setDeregisterTimeout)
            {
                DeregisterOnTimeout(ref component);
            }
            Console.WriteLine("[CS {1}]: Serwer dostał registra, komponent dostaje ID={0}", 
                component.ID, _id);
            var registerResponse = new RegisterResponse() { Id = component.ID, Timeout = 2000 };
            responses.Add(new Tuple<object, Type>(registerResponse, typeof(RegisterResponse)));
            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            return responses;
        }

        private void ResetComponentTimeout(Status status)
        {
            var activeComponent = RegisteredComponents.Find(c => c.ID == status.Id);
            if (activeComponent == null || activeComponent.TokenSource == null)
                return;
            activeComponent.TokenSource.Cancel();
            activeComponent.Threads = status.Threads;
            DeregisterOnTimeout(ref activeComponent);
        }

        private Tuple<object, Type> GenerateBackupServersMessage()
        {
            var backupServers = new BackupServers();
            backupServers.BackupCommunicationServers = _backupServers.ToArray();
            return new Tuple<object, Type>(backupServers, typeof(BackupServers));
        }

        private ICollection<Tuple<object, Type>> StatusCallback(object message)
        {
            var status = message as Status;
            var responses = new List<Tuple<object, Type>>();
            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            if (status.Id == _backupChildServerId)
            {
                //Current backup server
                Console.WriteLine("[CS {0}]: Backupowy serwer przyslal status", _id);
                ResetComponentTimeout(status);
                responses.AddRange(_messagesToSend);
                _messagesToSend.Clear();
                return responses;
            }
            else if(RegisteredComponents.Exists((s) => s.ID == status.Id &&
                        s.Type == Component.ComponentType.CommunicationServer))
            {
                //Not current backup server, our backupserver died
                Console.WriteLine("[CS {0}]: Backupowy serwer umarł, przyjmuje, ze moim nowym jest {1}",
                    _id, status.Id);
                _backupChildServerId = status.Id;
                responses.AddRange(_messagesToSend);
                _messagesToSend.Clear();
                return responses;
            }
            if (!RegisteredComponents.Exists(c => c.ID == status.Id))
            {
                //Komponent niezarejestrowany. Odrzucam połączenie.
                //bkula: Error message?
                var error = new Error() { ErrorType = ErrorErrorType.UnknownSender };
                return responses;
            }
            if(_isBackupServer)
                Console.WriteLine("[CS {0}]: Stałem się nowym masterem", _id);
            _isBackupServer = false;
            var activeComponent = RegisteredComponents.Find(c => c.ID == status.Id);
            ResetComponentTimeout(status);
            Console.WriteLine("[{0}] Przyszedł sygnał, nie deregistruje, timeout odnowiony", activeComponent.ID);
           
            //Console.WriteLine("[{0}] Server dostał StatusMsg i przesyła info o backup serwerach", activeComponent.ID);
            //     var availableThreads = activeComponent.Threads.Where(t => t.State == StatusThreadState.Idle).Count();
            //     if (availableThreads > 0)
            if (true)
            {
                switch (activeComponent.Type)
                {
                    case Component.ComponentType.TaskManager:
                        var problemToDivide = ProblemsToSolve
                            .Where(p => p.Status == ProblemStatus.ToDivide)
                            .FirstOrDefault();

                        var problemWithSolutions = ProblemsToSolve
                            .Where(p => p.Status == ProblemStatus.GetFinalSolution)
                            .FirstOrDefault();

                        if(( (problemToDivide != null && problemWithSolutions != null) && (problemToDivide.ID < problemWithSolutions.ID)) ||
                                (problemWithSolutions == null && problemToDivide != null))
                        {
                            var divideProblemMessage = new DivideProblem()
                            {
                                ProblemType = problemToDivide.ProblemType,
                                Data = problemToDivide.Data,
                                Id = problemToDivide.ID,
                                NodeID = activeComponent.ID
                            };
                            problemToDivide.TMID = activeComponent.ID;
                            problemToDivide.Status = ProblemStatus.Dividing;
                            responses.Add(new Tuple<object, Type>(divideProblemMessage, typeof(DivideProblem)));
                        }

                        if (((problemToDivide != null && problemWithSolutions != null) && (problemToDivide.ID > problemWithSolutions.ID)) ||
                                (problemWithSolutions != null && problemToDivide == null))
                        {
                            var partialSolutions = problemWithSolutions.PartialSolutions;
                            if (problemWithSolutions != null && partialSolutions != null)
                            {
                                var solutionMessage = new Solutions()
                                {
                                    Id = problemToDivide.ID,
                                    ProblemType = problemToDivide.ProblemType,
                                };

                                solutionMessage.Solutions1 = new SolutionsSolution[partialSolutions.Count];

                                int i = 0;
                                foreach (var sol in partialSolutions)
                                {
                                    solutionMessage.Solutions1[i++] = new SolutionsSolution()
                                    {
                                        TaskId = sol.TaskId,
                                        Data = sol.Data,
                                        Type = SolutionsSolutionType.Partial
                                    };
                                }
                                problemWithSolutions.Status = ProblemStatus.GettingFinalSolution;
                            }
                        }
                        break;

                    case Component.ComponentType.ComputationalNode:
                        var problemToCompute = ProblemsToSolve.FirstOrDefault(p => p.Status == ProblemStatus.ToCompute);
                        if(problemToCompute != null)
                        {
                            var partialProblemToCompute = problemToCompute
                                .PartialProblems
                                .FirstOrDefault(pp => pp.Status == ProblemStatus.ToCompute);

                            if(partialProblemToCompute != null)
                            {
                                var solvePartialProblemsMessage = new SolvePartialProblems()
                                {
                                    Id = problemToCompute.ID,
                                    ProblemType = problemToCompute.ProblemType,
                                    PartialProblems = new SolvePartialProblemsPartialProblem[1]
                                    {
                                        new SolvePartialProblemsPartialProblem()
                                        {
                                            Data = partialProblemToCompute.Data,
                                            NodeID = activeComponent.ID,
                                            TaskId = partialProblemToCompute.TaskId
                                        }
                                    }
                                };
                                partialProblemToCompute.NodeId = activeComponent.ID;
                                partialProblemToCompute.Status = ProblemStatus.Computing;
                                problemToCompute.UpdateProblemStatus();
                                //if(!problemToCompute.PartialProblems.Where(p => p.Status != ProblemStatus.Computing).Any())
                                //{
                                //    problemToCompute.Status = ProblemStatus.Computing;
                                //}
                                responses.Add(new Tuple<object, Type>(solvePartialProblemsMessage, typeof(SolvePartialProblems)));
                            }
                        }

                        Console.WriteLine("Server dostał StatusMsg i zwraca PartialProblems");
                        break;
                }
            }
            else
            {
                Console.WriteLine("[{0}] Brak wolnych wątków", activeComponent.ID);
            }
            return responses;
        }
        /// <summary>
        /// Solve request sent by CC
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private ICollection<Tuple<object, Type>> SolveRequestCallback(object message)
        {
            var request = message as SolveRequest;
            ulong id;
            if (request.IdSpecified)
            {
                id = request.Id;
            }
            else id = ++LastProblemID;
            var newProblem = new Problem()
            {
                ID = id,
                Data = request.Data,
                ProblemType = request.ProblemType,
                Status = ProblemStatus.ToDivide
            };
            newProblem.SolvingTimeout = request.SolvingTimeoutSpecified ? 0 : request.SolvingTimeout;
            ProblemsToSolve.Add(newProblem);

            _messagesToSend.Add(new Tuple<object, Type>(message, typeof(SolveRequest)));
            Console.WriteLine("[Id Problemu: {0}] Server otrzymał problem od klienta. Typ problemu: {1}",
                newProblem.ID, newProblem.ProblemType);
            var response = new SolveRequestResponse() { Id = newProblem.ID };
            var responses = new List<Tuple<object, Type>>();
            responses.Add(new Tuple<object, Type>(response, typeof(SolveRequestResponse)));
            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            return responses;
        }
        /// <summary>
        /// Solution request sent by a CC after sending a Solve request.
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        private ICollection<Tuple<object, Type>> SolutionRequestCallback(object message)
        {
            var responses = new List<Tuple<object, Type>>();
            var solutionRequest = message as SolutionRequest;
            var problemToSolve = ProblemsToSolve.FirstOrDefault(p => p.ID == solutionRequest.Id);
            /*
        if(!ProblemsBeingSolved.Exists(p=>p.ID == solutionReq.Id))
        {
            var error = new Error() { ErrorType = ErrorErrorType.InvalidOperation };
            responses.Add(new Tuple<object, Type>(error, typeof(Error)));
            return responses;
        }*/
            Console.WriteLine("[CS]: Klient się przypomina.");
            if (problemToSolve != null)
            {
                var solution = new Solutions()
                {
                    Id = problemToSolve.ID,
                    CommonData = problemToSolve.Data,
                    ProblemType = problemToSolve.ProblemType,
                    Solutions1 = new SolutionsSolution[problemToSolve.PartialProblems.Count]
                };

                int i = 0;
                foreach (var sol in problemToSolve.PartialSolutions)
                {
                    var type = sol.Status == ProblemStatus.Done ? SolutionsSolutionType.Final : SolutionsSolutionType.Ongoing;

                    solution.Solutions1[i++] = new SolutionsSolution()
                    {
                        TaskId = sol.TaskId,
                        Data = sol.Data,
                        Type = type
                    };
                }
                responses.Add(new Tuple<object, Type>(solution, typeof(Solutions)));
            }

            var backupServersMessage = GenerateBackupServersMessage();
            responses.Add(backupServersMessage);
            return responses;
        }


        private void AddDeregisterMessageToBackup(ulong id)
        {
            var deregister = new Register();
            deregister.DeregisterSpecified = true;
            deregister.Deregister = true;
            deregister.IdSpecified = true;
            deregister.Id = id;
            _messagesToSend.Add(new Tuple<object, Type>(deregister, typeof(Register)));
        }

        public void DeregisterOnTimeout(ref Component TMComponent)
        {
            var id = TMComponent.ID;
            TMComponent.TokenSource = new CancellationTokenSource();
            TMComponent.Token = TMComponent.TokenSource.Token;
            var token = TMComponent.Token;
            TMComponent.TimeoutDeregister = new Task(() =>
            {
                Thread.Sleep((int)_timeout + 4000);
                if (token.IsCancellationRequested)
                    return;
                if (!_isBackupServer)
                {
                    Console.WriteLine("[Id Komponentu: {0}] Przekroczono timeout w registrze. Wyrejestrowuje komponent.",
                        id);
                    RegisteredComponents.Remove(RegisteredComponents.Find(c => c.ID == id));
                    AddDeregisterMessageToBackup(id);
                }
            }, TMComponent.Token);
            TMComponent.TimeoutDeregister.Start();
        }

        private ICollection<Tuple<object, Type>> SolutionsCallback(object message)
        {
            var solutions = message as Solutions;
            var activeComponent = RegisteredComponents.FirstOrDefault(c => c.ID == solutions.Id);
            var problem = activeComponent.Type == Component.ComponentType.ComputationalNode ?
                ProblemsToSolve.FirstOrDefault(p => p.ID == solutions.Id && p.Status == ProblemStatus.Computing) :
                ProblemsToSolve.FirstOrDefault(p => p.ID == solutions.Id && p.Status == ProblemStatus.GettingFinalSolution);


            Console.WriteLine("Serwer otrzymał rozwiązania do wysłanych problemów");
            if (activeComponent != null)
            {
                switch (activeComponent.Type)
            //_messagesToSend.Add(new Tuple<object, Type>(message, typeof(Register)));
                {
                    case Component.ComponentType.ComputationalNode:
                        if (problem != null)
                        {
                            foreach(var solution in solutions.Solutions1)
                            {
                                problem.PartialSolutions.Add(new PartialSolution
                                {
                                    Data = solution.Data,
                                    TaskId = solution.TaskId,
                                    Status = ProblemStatus.GetFinalSolution
                                });
                                problem.PartialProblems.First(pp => pp.TaskId == solution.TaskId).Status = ProblemStatus.Done;
                            }
                            problem.UpdateProblemStatus();
                            //if(problem.PartialProblems.Where(pp => pp.Status == ProblemStatus.Done).Count() == problem.PartialProblems.Count())
                            //{
                            //    problem.Status = ProblemStatus.GetFinalSolution;
                            //}
                        }
                        break;
                    case Component.ComponentType.TaskManager:
                        if (problem != null)
                        {
                            problem.PartialSolutions = new List<PartialSolution>();
                            foreach(var solution in solutions.Solutions1)
                            {
                                problem.PartialSolutions.Add(new PartialSolution()
                                {
                                    Data = solution.Data,
                                    Status = ProblemStatus.Done
                                }
                                );
                            }
                            problem.Status = ProblemStatus.ReadyToSend;
                        }
                        break;
                }
            }
            _messagesToSend.Add(new Tuple<object, Type>(message, typeof(Solutions)));
            var responses = new List<Tuple<object, Type>>();
            return responses;
        }

        private void Delay(int arg)
        {
            int sec = arg * 1000;
            if (sec == 0) sec++;
            Thread.Sleep(sec);
        }
    }
}