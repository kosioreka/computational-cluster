﻿using Communication;
using System;
using System.Threading.Tasks;

namespace CommunicationServer
{
    class CSProgram
    {
        static int _port;
        static string _maddress;
        static int _mport;
        static uint _time =1;

        static void Main(string[] args)
        {
            GetArguments(args);
            if (_port == 0) _port = 13000;
            var myData = new ConnectionData("127.0.0.1", _port);
            ConnectionData masterData = null;
            if (_maddress != null)
            {
                masterData = new ConnectionData(_maddress, _mport);
            }
            var server = new CommunicationServer();
            server.Work(myData, _time * 1000, masterData);
            //StartMaster();
            //StartChild1();
            //StartChild2();
            //StartChild3();
            //StartChild4();
            Console.ReadKey();
        }
        static void GetArguments(string[] args)
        {
            for (int i=0;i<args.Length-1;i++)
            {
                var str = args[i];
                var str2 = args[i + 1];
                if (str.Contains("-port"))
                {
                    int.TryParse(str2, out _port);
                }
                else if (str.Contains("-maddress"))
                {
                    _maddress = str2;
                }
                else if (str.Contains("-mport"))
                {
                    int.TryParse(str2, out _mport);
                }
                else if (str.Contains("-t"))
                {
                    _time = uint.Parse(str2);
                }
            }
        }

        //static void StartMaster()
        //{
        //    var masterData = new ConnectionData("127.0.0.1", 13000);
        //    var master = new CommunicationServer();
        //    Task.Run(() => master.Work(masterData, 2000));
        //}

        //static void StartChild1()
        //{
        //    var masterData = new ConnectionData("127.0.0.1", 13000);
        //    var connectionData = new ConnectionData("127.0.0.1", 13001);
        //    var child1 = new CommunicationServer();
        //    Task.Run(() => child1.Work(connectionData, 2000, masterData));
        //}

        //static void StartChild2()
        //{
        //    var masterData = new ConnectionData("127.0.0.1", 13000);
        //    var connectionData = new ConnectionData("127.0.0.1", 13002);
        //    var child2 = new CommunicationServer();
        //    Task.Run(() => child2.Work(connectionData, 2000, masterData));
        //}

        //static void StartChild3()
        //{
        //    var masterData = new ConnectionData("127.0.0.1", 13000);
        //    var connectionData = new ConnectionData("127.0.0.1", 13003);
        //    var child2 = new CommunicationServer();
        //    Task.Run(() => child2.Work(connectionData, 2000, masterData));
        //}

        //static void StartChild4()
        //{
        //    var masterData = new ConnectionData("127.0.0.1", 13000);
        //    var connectionData = new ConnectionData("127.0.0.1", 13004);
        //    var child2 = new CommunicationServer();
        //    Task.Run(() => child2.Work(connectionData, 2000, masterData));
        //}
    }
}