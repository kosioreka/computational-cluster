﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using CommunicationServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication;


namespace CommunicationServer.Tests
{
    [TestClass()]
    public class CommunicationServerTests
    {
        [TestMethod()]
        public void CSPropertyRegisteredComponents_StoresCorrectly()
        {
            CommunicationServer cs = new CommunicationServer();
            cs.RegisteredComponents = new List<Component>();
            cs.RegisteredComponents.Add(new Component { ID = 10 });
            Assert.AreEqual(1, cs.RegisteredComponents.Count);
        }

        [TestMethod()]
        public void CSPropertyRegisteredComponents2_StoresCorrectly()
        {
            CommunicationServer cs = new CommunicationServer();
            cs.RegisteredComponents = new List<Component>();
            Component c = new Component { ID = 10 };
            cs.RegisteredComponents.Add(new Component { ID = 10 });
            Assert.AreEqual(c.ID, cs.RegisteredComponents.First().ID);
        }

        [TestMethod()]
        public void CSPropertyProblemsToBeSolved_StoresCorrectly()
        {
            CommunicationServer cs = new CommunicationServer();
            cs.ProblemsToSolve = new List<Problem>();
            byte[] arr = { 11 };
            cs.ProblemsToSolve.Add(new Problem { Data = arr });
            byte[] arr2 = { 11 };
            Assert.AreEqual(arr2[0], cs.ProblemsToSolve.First().Data[0]);
        }



        [TestMethod()]
        public void CSPropertyLastComponentID_StoresCorrectly()
        {
            CommunicationServer cs = new CommunicationServer();
            cs.LastComponentID = 10;
            Assert.AreEqual((ulong)10, cs.LastComponentID);
        }

        [TestMethod()]
        public void CSPropertyLastProblemID_StoresCorrectly()
        {
            CommunicationServer cs = new CommunicationServer();
            cs.LastProblemID = 10;
            Assert.AreEqual((ulong)10, cs.LastProblemID);
        }
    }
}