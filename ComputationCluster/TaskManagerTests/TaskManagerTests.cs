﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using TaskManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Communication;

namespace TaskManager.Tests
{
    [TestClass()]
    public class TaskManagerTests
    {
        [TestMethod()]
        public void TaskManagerConstructorTest()
        {
            TaskManager tm = new TaskManager();
            Assert.IsNotNull(tm);
        }

        [TestMethod()]
        public void delayTest()
        {
            TaskManager tm = new TaskManager();
            tm.Delay(2);
            Assert.IsTrue(true);
        }

        [TestMethod()]
        public void TMPropertyID_StoresCorrectly()
        {
            TaskManager tm = new TaskManager();
            tm.Id = 999;
            Assert.AreEqual((ulong)999, tm.Id);
        }

        [TestMethod()]
        public void TMPropertyTimeout_StoresCorrectly()
        {
            TaskManager tm = new TaskManager();
            tm.Timeout = 10;
            Assert.AreEqual((uint)10, tm.Timeout);
        }

        [TestMethod()]
        public void TMPracaTest()
        {
            TaskManager tm = new TaskManager();
            bool flag = Task.Factory.StartNew(tm.Work).Wait(TimeSpan.FromSeconds(1));
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void TMPracaTest2()
        {
            TaskManager tm = new TaskManager();
            bool flag = Task.Factory.StartNew(tm.Work).Wait(100);
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void TMSendRegisterMsgAndGetRegisterResponseMsgAsyncTest1()
        {
            TaskManager tm = new TaskManager();
            bool flag = Task.Factory.StartNew(tm.SendRegisterMsgAndGetRegisterResponseMsgAsync).Wait(TimeSpan.FromSeconds(1));
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void TMSendStatusMsgAndGetMsgAsyncTest()
        {
            TaskManager tm = new TaskManager();
            bool flag = Task.Factory.StartNew(tm.SendRegisterMsgAndGetRegisterResponseMsgAsync).Wait(TimeSpan.FromSeconds(1));
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void TMSendRegisterMsgAndGetRegisterResponseMsgAsyncTest2()
        {
            TaskManager tm = new TaskManager();
            Task t = Task.Factory.StartNew(tm.SendRegisterMsgAndGetRegisterResponseMsgAsync);
            bool flag = t.Wait(TimeSpan.FromSeconds(1));
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void TMSendRegisterMsgAndGetRegisterResponseMsgAsyncTest3()
        {
            try
            {
                TaskManager tm = new TaskManager();
                bool flag = tm.SendRegisterMsgAndGetRegisterResponseMsgAsync().Wait(10);
                if (flag == false)
                    Assert.IsFalse(flag);
                else
                    Assert.IsTrue(flag);
            }
            catch (AggregateException ae)
            {
                Assert.AreEqual("Sth bad happened during Register method execution.", ae.Message);
            }
            catch (Exception e)
            {
                Assert.Fail(string.Format("Unexpected exception of type {0} caught: {1}", e.GetType(), e.Message)
                );
            }
        }


        [TestMethod()]
        public void TMSendStatusMsgAndGetMsgAsyncTest1()
        {
            TaskManager tm = new TaskManager();
            Task t = Task.Factory.StartNew(tm.SendStatusMsgAndGetMsgAsync);
            bool flag = t.Wait(100);
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }

        [TestMethod()]
        public void TMSendPartialProblemMsgAsyncTest()
        {
            TaskManager tm = new TaskManager();

            Task t = Task.Factory.StartNew(() => tm.SendPartialProblemMsgAsync(null));
            bool flag = t.Wait(100);
            if (flag == false)
                Assert.IsFalse(flag);
            else
                Assert.IsTrue(flag);
        }
    }
}